「請稍等一下」

柔和的聲音響起，吾面前的空間扭曲著形成了1個少女的身姿。
銀色的頭髮長至腰際的少女穿著以黑色為基調的女僕服。
和還留有稚氣的臉相反，身體的線條非常富于起伏，畢竟胸很大而腰很緊繃。絕佳的身材。

「蕾娜嗎。怎麼了」
「路西法大人。在前往帝國的時候，稍微會有點問題……」

被稱為蕾娜的少女搭話的瞬間，從她站的地方升起了猛烈的火柱。猛地回頭看去，露米艾爾那像天使一樣可愛的臉因為充滿敵意的表情歪了。

「你是得到了誰的允許出現在Dar～ling面前的？你這狐狸精」

看到惡狠狠地說的露米艾爾的表情的話，如果是一般的魔族豈止是發抖，搞不好會由於那從身體裡發出的壓倒性的神氣的影響而飛散得無影無踪吧。
可是，以華麗的動作躲開了突然襲擊的火柱的女僕少女輕輕站到吾的旁邊纏上手臂。

「您忘了嗎，露米艾爾大人。我也是路西法大人的妻子哦？ 沒有理由不站在煩惱的丈夫旁邊，我認為不需要得到誰的許可哦？」
「你竟然以婢女的身份自稱妻子？」
「誒誒。因為，承認我是『最愛的妻子』的就是路西法大人。吶？ 親·愛·的」

蕾娜那樣說後，一邊深紫色的眼睛妖艷地放著光輝一邊把吾的手臂推到自己的胸上。豐滿的實在是絕佳的觸感。
不瞞你說，她也是吾的妻子。露米艾爾是第一夫人，蕾娜相當於第三夫人。
不過，露米艾爾討厭蕾娜的理由並不是吾有其他的妻子。

「即使是現在我也不認可！ 光是看到你的身影，『那個時候被砍飛的右手』就痛了！」
「啊啦啊啦……那不是起因於露米艾爾大人太弱了嗎？ 手被砍飛時候的露米艾爾大人滑稽的表情即使是過了500年的現在我也清楚記得哦」
「啊啦，是嗎。那麼，現在在這裡繼續那個時候的事？ 也可以哦？」
「我是沒關係哦？ 看露米艾爾大人的醜態最愉快了。又會讓您哭的吧？」

咣！ 的發出可怕的聲音露米艾爾把桌子打破了。不如說是粉碎了。
這可有點不妙吶。吾稍微嘆氣之後說。

「安靜」
「可，可是，Dar～ling」
「好了安靜。蕾娜，你明白吧？」
「是的。不論發生什麼我都是路西法大人忠實的僕人。不會違背您的命令」

和不滿快要爆發的露米艾爾對照鮮明的，蕾娜看上去態度很爽快。
……這個蕾娜正是500年前來討伐吾的那個勇者本人。
以她率領的大軍為對手，造成了和露米艾爾她們攻進來的時候差不多的損失。

不止進入了戴涅布萊領內，將有名的魔神們一個接一個打敗後甚至侵入了這個宮殿內的時候的樣子吾現在也還記得。
然後雖然被帶入了1對1的對決，但結果嘛……注意到時吾又把她強暴了。
到馴服為止好像花了相當久的時間但吾不太記得了。總而言之，注意到時那樣充滿了憎恨的蕾娜染紅了臉頰以濕潤的眼睛仰視著吾。

喜歡上她的吾決定把她作為第3位妻子迎入，被砍飛了手的露米艾爾對此當然激烈反對並大吵了一架。那時候哄她可是相當辛苦。
就是這樣，變成吾的第三夫人的蕾娜一邊作為妻子一邊扮演著女僕的角色。據說理由好像是成為勇者前她一直在哪裡的小公館裡作為女僕生活著，所以無論如何都想照顧吾。之後500年間她經常在吾旁邊侍立。不論發生什麼。當然也聽見了剛才的對話所以這樣出現了吧。

「那麼，蕾娜。既然你闖進來了就說明你聽到對話了吧？ 你怎麼想？」
「我覺得是極好的判斷。沒有從帝國來勇者已經500年了。一段時期對討伐路西法大人那樣傾注了心血的帝國現在在想什麼我很好奇。只是」
「只是？」
「不知道您還記不記得這種小事，最初來討伐路西法大人的勇者的死因您還有記憶嗎？」

最初攻來的勇者嗎。記得是在將近1000年前。露米艾爾被吾反殺被俘了，但那是最開始的開端吧？ 老實說不太記得了。
那時候是怎樣解決的呢。

「是鼻息哦」

到剛才為止一直沉默著的露米艾爾愉快地說道。

「Dar～ling『哼！』的一呼吸，勇者那孩子就被吹飛了砸在宮殿的牆上死了不是嗎」
「啊啊，對。說起來也有過這種事吶」
「是的。我感覺他第一次聽路西法大人說話的時候身體就在發抖。然後接著被送來的勇者怎麼樣了。您還記得嗎？」

「……吾記得，好像瞪眼的瞬間就不動了」
「對對。Dar～ling只是稍微注入殺意瞪了眼就死了呢。心臟破裂了」
「沒錯！ 路西法大人只靠『眼力』就輕鬆地屠殺了身為勇者的人！」

為什麼你那麼開心，蕾娜？ 同胞死了不痛心嗎吾稍微有點擔心了。哦，是原同胞。
嘰嘰喳喳地興奮著的蕾娜突然表情認真地注視吾。


「那樣的路西法大人前往帝國的話您覺得會怎麼樣呢？ 您靠鼻息和眼力就能殺掉勇者哦？如果是普通的人類的話，光是您恰巧路邊附近就會因為受不住魔力的壓力身體爆炸四散」

確實。即便是在至今為止的戰鬥中也有過幾次作為勇者的輔佐跟來的帝國軍的人類們只是看見了出現在戰場上的吾的身影，就從身體裡噴出血死了。

「因此，如果現在的路西法大人就這樣前往帝國的話，僅僅只是站著大半平民就會死絕變成一副地獄圖，變得沒時間去看勇者的可能性很高」

麻煩了。雖然只是想去看一眼。但吾是預定短暫花上幾天到幾年左右。
可是光走路就會把人類們逼入死路也不有趣，各個國家的反抗必然也會變強烈。森精靈和龍都團結起來襲擊過來的話稍微會有點麻煩吶。

「那麼，吾該怎麼辦才好？」
「Dar～ling和我一起相愛就好了哦！ 像至今為止一樣打情罵俏，繼續甜蜜的生活吧？」
「您不需要傾聽那個色情魔說的話。在這裡我有一個想法。開門見山地說，路西法大人變成人類去帝國不就行了嗎！」

「吼。吾變成人類」
「喂！？ 你想讓Dar～ling變成和庸俗的人類們一樣！？」
「露米艾爾，稍微閉嘴」
「……唔～……！」

如果吾就以這樣的姿態去的話即使抑制了力量也會讓人類們遭受不必要的損失。好像只是稍微打個噴嚏就會不得了了。
可是如蕾娜所說變成人類的話那種擔心也沒必要吧。因為現在明明勇者們擁有驚人的力量，卻在人類之中沒問題地出生成長了吶。

「變成了人類的話，從身體裡發出的魔力的量必然會被抑制。之後為了那個『化妝』不被突然揭下，只要這邊調整下路西法大人也能變成非凡的人類，這是我的愚見」
「原來如此。好吾知道了。趕快變變看吧」

怎樣的姿態好呢。

「喂，你們怎麼想？ 吾用怎樣的姿態去比較好？」
「是是！ 我覺～得就像現在一樣令人陶醉的非常帥的Dar～ling的樣子就好！  稍微有點長的耳朵還有稍微長出來一點的犬齒都好可愛！ 然後鮮紅的眼睛棒極了！呀！ 想抱了！」

唔姆，那微妙地不是人類吶。
吾熬煉魔力做出穿衣鏡，照出了自己的身體。
漆黑的頭髮紅色的眼睛，輪廓清晰的容貌加上高個子。穿在身上的是黑色的服裝和紅色的披風。
沒錯，吾的姿態相當像人類。如果沒有露米艾爾列舉的特徵的話都會分不清。

「說的是呢。只有這次我也想贊成露米艾爾大人的意見……不過雖說經過了500年，但也未必沒有長壽而且還記得路西法大人的英姿的種族。我覺得果然這裡還是應該變成完美的人類」
「嘛，也是呢。可能的話不會被警戒比較好。……變成無害的老爺爺試試吧」
「不要！！」
「不行！！」
「哦，噢？」

她們罕見地傳來了相同的意見而且有些焦躁。怎麼，討厭老爺爺嗎？

「Dar～ling保持又年輕又帥的姿態比較好！ 我不想看見變成了老爺爺的Dar～ling！」
「露米艾爾大人所言極是！ 路西法大人變成蹣跚的老人什麼的……嗚呼，淒慘也該有個度！」

那麼，那什麼。年輕比較好嗎。說到年輕又不會被警戒的姿態的話……。

「那麼，少女……不，變成幼女嗎」
「Dar～ling這個笨蛋！！」
「路西法大人這個傻瓜！！」
「怎麼了你們！？」

「Dar～ling是又年輕又帥的男人才好！ 女人什麼的我絕對不認可！」
「意見完全相同！ 如此造型完美的路西法大人變成那種隨處可見的小女孩太可嘆了！！」

真是幫任性的傢伙吶。這樣的話結果不是已經只有年輕的男子了嗎。和現在差不多啊。
需要稍微試著改變造型吧。試著想像了普通的人類。給人隨處可見的感覺的那種。
唔～姆。那麼，這個怎麼樣。

「……等下。稍微改變下姿態試試」

吾試著用魔術迅速改變相貌之後說。

「呋，這樣怎麼樣」
「……」
「……」

2位妻子投來了像在看十分尷尬的東西一樣的視線。

「說點什麼！！」
「因為，那個……有點微妙吧」
「路西法大人的容顏變得令人遺憾了……」

這幫女人真的是……！
……嘛算了。這也全是為了愛吾的可愛的妻子們。先慎重地聽她們的意見吧。
所以，那之後持續了3天容貌的商量。


「這個怎麼樣」

吾在露米艾爾和蕾娜面前轉了一圈給她們看。

「從我的角度看是相當有滿足感的！ 有60分！」
「嗯嗯……嘛，因為很可愛所以行吧？ 35分」

這不是很低嗎？ 不禁快要發牢騷了但是不忍耐不行。
雖然被隨意擺弄了個夠之後受到了微妙的評價，但吾重新確認了自己的身姿。

髮色是鮮艷的青色，眼睛是翠綠色的瘦長臉的少年。年紀作為人類也就大概15、6歲吧。
因為被說了聲音有點低所以也發出了像少年一樣多少有些高的聲音。
身高說是太高了就調整了，調到了比蕾娜稍高的170cm。
從哪裡看都是一副文雅的男子的樣子。雖然就吾個人來說不太喜歡，但是嘛已經夠了。調整好累。

「好，那麼就趕快前往帝國吧」
「請等一下！」
「啊，又怎麼了！？」
「這個說話方式！ 像現在這樣混合了可愛和凜然的美少年用這種說話方式明顯不自然」

「庫……那麼，怎麼辦才好」
「我來講課。從舉止到各方面，都交給我吧」

真是隨便她了。


調整到蕾娜感到滿意的結果，『我』決定前往帝國了。
