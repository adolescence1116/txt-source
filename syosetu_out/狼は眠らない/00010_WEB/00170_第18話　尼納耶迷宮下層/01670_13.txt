斯諾舉著奇特的武器。
此時雷肯注意到了。

（是沃卡領主拜託希拉訂購的那個放出火魔法的武器）

不知道為什麼這男人會有這種東西。
總之，斯諾現在正打算從馬車裡，對位在車夫座的雷肯再次放出火魔法。

一瞬，雷肯打算放出〈炎槍〉，但有可能把血灑在赫蕾絲身上，便放棄了。

「〈風阿〉！」

突風產生，武器被從斯諾的手中吹飛了。

「阿。」

像是要追趕從窗戶飛出的武器，斯諾的手伸了出來。
雷肯上前抓住那隻手，然後往山谷的方向拉去。

「嗚哇。」

門被撞破，斯諾飛出了馬車。
此時，武器已經掉進溪谷了。

千鈞一髮之際，斯諾踩在地面上踏住了。還有半步就是懸崖。
雷肯也跳下了車夫座。

「你，你這傢伙。我是何許人也，你不知道嗎？對本人，斯諾・派澤倫做這種事，你以為能輕易了事嗎。」
「你才是，做這種事，以為能輕易了事嗎？」

交談的同時，雷肯也毫不大意地以〈立體知覺〉搜索著馬車內部。

沒問題。赫蕾絲安然無恙。
槍使男也是，似乎昏了過去，身體靈巧地彎折，動也不動。

「現在還能原諒你。」

斯諾一邊說著一邊拔出劍，然後直接砍向雷肯。

速度很棒的拔刀術。
只有這點不得不承認。
雷肯迅速向後退，躲開斯諾的劍，然後發出咒文。

「〈風阿〉！」

斯諾為了使出拔刀而向右前方踏出的右腳，連同腳邊的土一起承受了來自左方的突風。失去平衡的斯諾，向山谷的斜坡倒去。

「〈炎槍〉！」

雷肯放出的攻擊魔法，應該會貫穿斯諾的胸口才對。
但是，被彈開了。
應該說，消散了。

（有穿戴對魔法防禦的裝備嗎）

正要站穩姿勢時受到了衝擊，斯諾控制不住身體，隨著腳邊崩塌的土，跌向溪谷去了。

雷肯瞧了瞧馬車內部。
明明發生了如此騷動，但赫蕾絲沒有醒來的跡象。
把槍使的身體拖出來，然後直接丟進了山谷。

在乘車席的後側，赫蕾絲躺在那裡。
彎曲著腳的姿勢，看起來睡得很不好，但撥開了艷麗的頭髮一看，正靜靜沉睡著。
今天的赫蕾絲，不同於鎧甲之姿，身穿著整潔的襯衫，以及輕飄飄的裙子。

「赫蕾絲。」

叫了也沒有任何反應。
雷肯從〈收納〉取出黃色藥水，灑在赫蕾斯的臉上。
不久後，赫蕾絲取回了意識。

「嗚─嗯。」

注意到起來後亂動會有危險，雷肯便抱起赫蕾絲，移動到了馬車後方。赫蕾絲的〈箱〉也在馬車裡，便以〈移動〉運了過來。
搬運途中，赫蕾絲微微張開了眼。

然後，注意到眼前是雷肯的臉，破顏一笑。

「被救下，了阿。」
「啊啊。能走嗎。」
「⋯不。好像不行。不好意思阿，能就這樣搬著我嗎。」
「這樣阿。」

雖然解除了異常狀態，但麻痺還殘留在體內的樣子。

是具有詛咒的武器嗎。
那麼用〈哈爾特之短劍〉劃傷赫蕾絲就行了。

是這樣沒錯，但是。

（原來是這麼有女人味的身材嗎）

受到鍛鍊的身體，能譽為少年般地水嫩，而胸部與臀部的隆起，又散發著成熟的女色。
纖細的腿，是無垢而煽情，眼前的喉嚨與鎖骨，和雷肯之間沒有任何阻礙。
沒有化妝的臉龐細緻平滑，微張的雙脣嬌豔欲滴。
如此美麗的身體，不想以刀刃傷害。
而且，也不知道究竟是不是詛咒。
總之趕快帶回去，接受〈淨化〉是最好的。

雷肯中途先讓赫蕾絲躺在斜坡的草皮上，把劍、守護石和盾收進〈收納〉。也收了赫蕾絲的〈箱〉。
然後再次抱起赫蕾絲。

「抓緊了。」

赫蕾絲讓纖細的手臂，緊緊纏繞在雷肯的脖子上。
柔和的香氣，輕柔地佔滿了雷肯的鼻腔。
接著，雷肯宛如在空中飛行，溫柔地搬運赫蕾絲。

開始移動後，也想過用馬車就行了，但跑到街裡的話，也會有人認得那馬車，並非一定不會有麻煩事。
而且比起乘坐馬車，這樣更快。

「原來有男人，能將我，像這樣輕盈地，宛如少女似地運送阿。」

懷中的女性如此嘟囔，但被風給消去，沒有傳達到雷肯的耳中。