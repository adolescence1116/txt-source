妮姫、雷肯和艾達回到了作業房喝茶。

「哎呀，真帥氣的人呢。結果卻被欺負成那樣。雷肯，你是魔鬼嗎！」

說要戰鬥的是妮姫。雷肯看向妮姫。

「被知道我在這裡了呢。這下，每天都會來吧。」
「那個騎士大人，感覺對妮姫小姐很感興趣呢。兩人之間到底有過什麼呢？」
「不，也沒什麼就是了。」

妮姫簡單地說明了事由。

三年前，通往王都的通路上出現了銀狼。這條路不能自由通行的話，會對沃卡城產生重大打擊。很快地，從守護隊編成了討伐隊，並向銀級冒険者提出了討伐的緊急委託，但當時的兩位守護隊隊長受了重傷，銀級冒険者們也受到了不少損傷。

領主克里姆斯・烏魯邦找了藥師希拉商量。希拉派了孫女妮姫過來。對於這看起來頂多十八歳的冒険者，克里姆斯感到相當失望。但終究不能放著銀狼不管，便再度召集了冒険者們，與妮姫一起派遣到了通路。

妮姫一刀切斷了銀狼的兩支前腳，接著一刀取下了首級。同行的銀級冒険者做了證言，克里姆斯也只有相信一途。當時，沃卡冒険者協會認可了妮姫為銀級冒険者。
然後兩年前，同樣是在通往王都的路上，這次出現了八目大蜘蛛。妮姫再次前去討伐。同行的冒険者還沒出手，討伐就迅速地結束了。

對於這項功績，沃卡冒険者協會贈予了金級作為回報。

「那個，這段話，沒有出現剛剛那位騎士大人阿。」
「討伐八目大蜘蛛的時候，作為監視員一起同行了。嘛，是想讓他看看現場的父母心吧。不過，明明不在那種立場，卻發出意義不明的指示害現場混亂，結果最後在魔獸出現時，腳都軟掉了呢。」
「然後在眼前，巨大的魔獸被一刀兩斷了對吧。」
「嘛，算是吧。」
「那一劍，也把騎士大人的心臟給一刀兩斷了呢。」
「一刀兩斷是那樣用的嗎？不過阿，一次的話到還好，但兩次，就不會是偶然了。」
「與騎士大人的邂逅嗎？就是所謂的命運對吧！」
「不是那邊喔。」
「魔獸的出沒，不是偶然，的意思嗎？」
「九成九跟〈催眠〉使有關吧。」
「不是〈調教〉嗎？」
「能對那種高位魔獸使用〈調教〉的調教師，應該很少見吧。〈催眠〉使之中，有以人類為對象的，跟以動物和魔獸為對象的。下〈催眠〉比下〈調教〉容易。雖然做不到複雜的命令，也建立不了信賴關係，只能當棄子用就是了。」
「什麼東西？〈催眠〉？誰睡著了嗎？」
「你沒在聽人說話嗎。」
「雷肯，那種說法可不好喔。不會去深思別人說的話，可是小艾達的優點呢。」
「沒錯！」

（根本不是在稱讚喔）

「話說回來，那樣的每天都來的話可受不了阿。一不小心，不，就算沒有不小心，也會被米多斯可給嗅出來吧。那可，不妙。」

妮姫思索了一會後，看了雷肯和艾達的臉。

「小艾達。你有接明天的委託嗎？」
「今天才剛回來喔。明天還沒有預定。」
「好。二位，先在這裡等等阿。」

說完，妮姫就出去了。
雷肯雖然想練習〈回復〉，但艾達不放雷肯自由。

「我做了些什麼，有沒有興趣阿。」
「沒有。」
「收下了你給的〈伊希亞之弓〉之後，首先接了附近的魔獸討伐，然後成功了。」
「沒有給你。」
「接著是接了附近的護衛委託，當然也成功了，還拿到高評價喔。」
「你真的沒在聽人說話阿。」
「在那之後，接了比較困難費時的護衛委託。不用說，也成功了。」
「把弓還來。」
「然後這次呢，在護衛六個人的工作，接下了副隊長的位置。現在，在沃卡的冒険者之間，我被稱作是〈千擊的艾達〉。」
「你自己報上的名吧。」
「我秀出〈燈光〉和〈著火〉的時候，大家超驚訝的喔。說我很厲害呢。」

說著這些話時，妮姫回來了。

「稍微出去旅行一下吧。我接了三人的護衛工作。目的地是交易都市邦塔羅伊。來回要十四天的工作。雷肯，小艾達，明天早上，到南門旁集合喔。」