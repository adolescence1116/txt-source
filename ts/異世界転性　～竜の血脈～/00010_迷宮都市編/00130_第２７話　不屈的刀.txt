卡洛斯和露露脫隊時、說實話、薩基也快不行了。

到這裡為止雖然有苦戰、但也沒像這次這樣絶望過。薩基的絶招給敵人帶來大損傷、莉雅的刀沒有斬不了的東西。
但是現在哪個都沒有用。
也試過朗基努斯、充其量只有像被針刺到一樣的傷害。

雖然、還有最後的絶招。但是只要一使用、魔力就會耗盡。這樣就沒辦法保持加速的使用。
莉雅姑且不說、就算使用加速紀咕還是艱難的躲開巨人的攻擊。然而只要受到一擊、就算是紀咕堅韌的肉體、應該也會馬上變成肉餡。
死了也沒關係。看到卡洛斯和露露消失的身影、的確和瑪露說的一樣、應該不用懷疑。

但是、討厭死掉。
薩基有死掉的記憶、特別的這樣想著。
瞬間死去倒還好、萬一是四肢都被切斷的話、那是多麼痛苦的記憶。

「歐捏醬！」

撤退吧。現在的話還可以。
雖然對不起卡洛斯和露露、鎧和杖放棄吧。以現在的收入來考慮的話、之後還可以當笑話來講。

有這樣的想法的、是沒有持有戰士之血的人。

瑪露和薩基有同樣的意見。現在趕快逃的話。雖然重生過、但死掉瞬間的痛苦不想再次體驗。
但是不會逃。莉雅在戰鬥時不會逃。

戰士二人不考慮逃跑的事
自己死的話瑪露就帶薩基逃吧。這樣想著後、就能專心的戰鬥了。
殺死眼前的敵人。只是想殺掉。就這樣想著而已。
戰士的本能來說、就像是動物的本能吧。或是更原始的本能吧。

莉雅思考著。這才是真正的互相殘殺。

前世的決鬥。奧加王的戰鬥。這些都是有生命危險的、即使是圈套也戰鬥著。
當然血像是咕嚕咕嚕地被煮開一樣、心動的戰鬥著。

但是不同。

至今階層的戰鬥、雖然有苦戰但最後還是勝利了。

但這次不同。

計算無能為力。就算制定作戰計畫、但也失敗了。
それでもひたすらに、殺したい。自分の生き死にを考えず、殺意だけが高まっていく。

「いえああああっ！」

ありとあらゆる技術、力、全てを込めて刀を振り下ろす。

巨人的腳趾、在砍到一半時。

承受不了、刀破碎四散。

然而還沒全斷。莉雅繼續使用魔力、但最後承受不住斬擊的衝擊、鋼從內部爆炸。
但也切下了巨人一根指頭。

───

巨人咆嘯著。胡亂的暴動著。

雖然是製作堅固的迷宮、但到處都在碎裂。
受到破片的衝擊、紀咕的動作變慢了。巨人的拳頭就這樣落下。

然而紀咕就沒這麼幸運。沒有當場死亡。
遭受那一拳的幾秒後、痛苦持續著。

薩基見機行事。把先前回收的秘銀門、當作盾來使用。
和瑪露肩並肩、看到紀咕發出光芒消失了。
再一次回收門、現在逃離為第一考量。幸運的是出口並沒有關上。

之後是薩基最後的王牌。
拿起在迷宮中滾動的石頭。朝巨人的巨體看去。
眼睛似乎是弱點。只有一顆、確實是弱點吧。但沒辦法造成致命傷。
腦或心臓。心臓吧。如果打中腦袋、能想像到巨人暴怒的身姿。

「歐捏醬！最後的王牌！如果使用、魔力就會被耗盡！」

在腦海中構成術式。提升著魔力。精神集中。施展失敗自己就會死。
朝著手中的岩石集中。目標是巨人的心臟。

「爆裂轉移」

手中的岩石消失了。

固體直接轉移到固體裡。拿動物實驗時、往頭內轉移石頭。就這樣單純的死去。
但實際上爆發時。物質和物質會重合在一起、僅僅只有給予衝擊而已。
暴風往全身打時、就會好幾天躺在床上。魔力才會慢慢回復。

現在、又再次使用。

巨人的胸口那發生爆炸。
而巨人腰上的東西落下。鮮血飛散著。

「不行嗎⋯」

巨人的胸口和手來看。的確有造成傷害、那是之前沒有的傷。但也到此為止了。
薩基最強的魔法、沒有傳達給巨人。胸口的傷和手上的傷、絲毫沒有讓動作變慢。

之後只要吸引注意。
對著巨人被切斷的指頭的傷、莉雅取出戰斧攻擊著。
給予疼痛。然貌似沒有造成傷害、但還是會痛。
即使失去一根指頭也不會死、所以基本上是無視的。

巨人彎腰。像是要防護傷口的動作。弱點的眼睛也接近著。
莉雅投出槍。數打的槍。王城嚴選的槍。
但都略為偏離眼球、只造成些微的傷害就落到地面。

「可惡」

莉雅咒罵著。要害慢慢地靠近著。但是最信賴的太刀已經沒了。
從袋子裡拿出劍。秘銀制的劍。雖然硬度和鋒利比不上、但只要注入魔力也還算堪用。
全力地注入魔力朝巨人的腳打去、鋒利度不夠。刀的綜合性差不多。充其量只有不會爆炸的優點。

以身體輕的優勢、玩弄著巨人。如果之前的攻擊有效的話、早晚會耗盡力氣倒下的吧。

極限的往劍注入魔力、不斷著巨人的指頭揮下。
又一根、指頭落下。然後劍也碎了。

這場戰鬥結束後、回去認真的挑把好劍吧。莉雅心裡這樣決定著。

揮舞著戰斧給巨人手上造成傷。
給予疼痛。但無論累積了多少、巨人還是不倒下。
即使如此也繼續戰鬥著。僅僅是為了要戰鬥而活著一樣。

加速魔法被切斷、巨人的手揮了過來。身體強烈地撞上石壁。
但從那裡一口氣跑了出來、再次揮下斧頭。握柄彎曲報廢。再次受到攻擊。用受身承受住。

這種攻擊真的很不妙。之後只能忍住了。
肉、骨、磨合度提高著、還可以忍耐。痛覺耐性桑、拜託再堅持一下。

啪踏、身體傾斜了。加速被切斷。
全身都在疼痛著、還能動。還能戰鬥。
吶喊著、拿起替換的斧頭揮下。

但還不行。武器太弱。被彈了開來、鋼鐵彎曲了。

敲擊著直到毀壊。

手掌、敲到破裂。不知不覺、動作變慢了。
但是還能動。
肉體強化桑、骨格強化桑、內臓強化桑、工作辛苦了。

莉雅在破面的地面移動著、不愧是巨人、沒想到真的能破壊。
拳頭破壊著地面。破片飛散著、侵入了莉雅的鎧甲、衣服被撕的破碎。

又被摔到牆上。筋肉咯吱咯吱的扭曲了。嚴重出血。普通人的話早就內臓破裂死亡了。
但是還能動。

用刀拄著站了起來。眼裡的戰闘慾望還沒消失。
死嗎。這已經不重要了。

用刀拄著站了起來。眼裡的戰闘慾望還沒消失。
死嗎。這已經不重要了。

再生著。治癒著。天賦解放著。但是還不夠。
只要巨人沒有倒下、就還不夠。

搖搖晃晃走過來的渺小存在、巨人握了過去。

「歐捏醬！」
「莉雅醬！」

破破爛爛渾身是血、大概快不行了的渺小生物、巨人吃了下去。
眼前是巨大骯髒的牙齒、莉雅笑了。

「笨蛋」

朝著巨人捏住的左腳、自己用刀砍斷。
自由的落下、進到了巨人的口中。在被牙齒咬碎前、向著深處闖去。
巨人的喉嚨動著、莉雅被咽下。不、是被咽下。


莉雅的一隻腳落到地面。漂亮的切斷面。

巨人壓住自己的胸口。露出苦悶呻吟。

巨人的肚子裡、莉雅充份的鬧騰著。
使用火球魔法、照亮了滑溜溜的內臟、有著少許的厚刃的武器。
一隻腳沒了、只剩一隻、刺出武器把自己固定。
被吐出來就完了。再也沒有機會了。體力也所剩不多。
黏液和酸液、鎧甲和衣服被溶解著。但是莉雅不為所動。

有酸耐性真是太好了。

巨人痛苦著。
沒有暴動的餘裕、痛苦著。甚至抓傷了自己的胸口和肚子。不久、倒了下去、腳難看的動著、像是要用銳利的爪子抓開自己胸口一樣。

而薩基和瑪露驚恐地看著這幕。

巨人的動作越來越慢、不久後變成痙攣、然後就完全不動了。

做到了。

「從內側做的話。那樣也能打倒巨人」

呆然的薩基嘟噥著。簡直不敢相信。一寸法師喔。難道真的是這樣、把自己的腳砍斷、到那裡去嗎。

仰面倒下巨人的肚子、猛然的用刀刺下去。
是死掉而失去硬度了嗎、肉輕易的被切開了。
內臟的氣味、出現肉塊。長長的黑髮、塗滿了血和酸的莉雅。

替代了拐杖的刀、無法承受過度的酸折斷了。
剩單腳的莉雅、從巨人的肚子滑落。

「莉雅醬！」

瑪露急忙地跑了過去。薩基的腳使不上力、搖搖晃晃地跟了過去。
荒亂的呼吸。一隻眼睛被酸蝕而看不見。儘管如此還是對著二人的身影、她說著。

「我、勝利了」

之後就失去了意識。