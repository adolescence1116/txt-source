鶫準備在傍晚出院，正在收拾房間。因為會由醫院準備新的衣服，所以只要整理好行李就可以出院了。

因為曾穿著的衣服沾滿了血，所以決定廢棄掉了。不是什麼特別貴價的東西，而且在電視上播出過的衣服已經不能再穿了。本是很中意的，可惜了。

千鳥好像要和因幡一起去趟政府才回家，在鶫做完訊問後就馬上離開了醫院。恐怕是合約內容中的派遣工作吧，果然還是有些擔心。

順便提一下，當鶫回去之際，似乎為他準備了一種從外面看不到裡面樣子的車。好像是作為傳媒的對策。據說，即使過了一天，醫院門前還是擠滿了傳媒記者，一般的出租車似乎無法正常出入。添麻煩也有個限度吧。

⋯⋯在這次事件中最為惡質地引人注目的，肯定是直接打倒魔獸的鶫了吧。並不是魔法少女的普通男性打倒了魔獸──作為傳媒絕對很想採訪。或許，政府也有考慮到這一邊的情況。

與『葉隠櫻』的容貌相似的事，雖然不知道是被怎麼看待的，只能祈禱但願不會成為話題了。無論如何，是麻煩的事情終究沒變。

當鶫泄氣地坐到床邊，「喀喀」的，響起了敲門聲。好像有誰來了。

「好的，請進。」

──反正又是朝倉來消磨時間吧。當鶫一邊這樣想著一邊打開門，在那裡的是出乎意料的人物。

「誒嘿嘿，人家來啦」
「傷沒事吧？」

一邊這樣說著，鈴城和壬生一邊連回覆也不聽就進入了病房。鶫傻傻地張著嘴，呆望著倆人。

「為、為什麼會來這裡？」
「什麼嘛，就不能來嗎？」

鶫一如此問道，壬生就不滿地嘟著嘴坐到鶫旁邊。

「倒也不是那樣⋯⋯⋯只是因為沒想到六華的兩位會來啦」

既然事件已經結束了，她們就沒有跟鶫扯上關係的理由。說到底才剛剛再次被選作六華。可不認為有跟鶫談話的空閒。

「哼～？明明是一起戰鬥的關係，卻很冷淡呢。──說起來，已經不用戴太陽眼鏡了嗎？」
「⋯⋯啊！」

雖然鶫一下回神捂住了臉，但已經晚了。仔細想想的話，在這兩個人面前可從來沒有露出過本來面目。雖然主要是因為說明太麻煩了，但到底她們會怎麼看待這個和葉隱櫻相像的臉容呢？

正當鶫不知如何開口時，坐在旁邊的壬生就徐徐地伸出手，開始吧嗒吧嗒地觸摸鶫的臉。並且，把臉湊近到呼氣可及的程度，目不轉睛地凝視著鶫。

「原來如此呢。雖然千鳥說是似乎沒有血緣關係的，但既然如此相似也難怪想隱瞞啦」

這樣一邊一個人擅自理解，壬生一邊繼續說道。另一方面，鶫則是坐立不安。由於異性的突然接近而動搖，口齒不清起來。況且，在眼前的是可愛的女孩子。鶫變得不知所措。

當鶫僵固著任由壬生為所欲為，鈴城就發聲制止了。

「啊─，百合醬。就到此為止放過他嘛。鶫君臉都變得紅透了啦」
「嗯？為什麼？昨天可是看到我的內衣身姿也若無其事的，換這種程度就害羞也太奇怪了吧」
「不，你看，那時畢竟是緊急情況嘛。差不多感覺有點可怜了，就放過他，住手吧。己經好像要哭了啦」

鈴城這麼一說，壬生就一臉不情願的樣子把手抽離。鶫一邊鬆了一口氣，一邊雙手捂住了臉。微微發熱的自己的臉蛋真可惡。

⋯⋯雖然並沒有哭，但的確是很為難。鶫向鈴城「謝謝」的道謝，「咳咳」的清了清嗓子，重新面向倆人。耳朵還有一點泛紅是其可愛之處。

「⋯⋯那麼，要事是？」

鶫如此詢問後，鈴城就悠悠地開了口。

「我是想來道謝的。因為結果來說，是被你和千鳥醬救了呢。──真的，非常感謝」
「嗯嗯。大概沒有你們倆的話我們就已經死掉了。真是幫大忙了喲」

對這樣說著低下了頭的兩人，鶫驚慌失措地搭話。

「拜、拜託抬起頭來吧。要道謝的反而應該是我們吧。如果沒有六華的兩位在，最糟的情況下我們說不定都已經死了。協助立於鋒尖浪頭的兩位是理所當然的。⋯⋯而且，我之所以能打倒魔獸，也是有賴兩位讓魔獸虛弱了啦」

──單憑鶫一個人的力量，是無法打倒那頭魔獸的。這樣想來，那肯定不是鶫的功績，而毫無疑問是作為團隊的功績。正因如此，可沒有理由被如此感謝。

鶫如此主張後，兩人就面面相覷，開始笑了起來。

「啊哈哈！和千鳥醬說了同樣的話呢。果然是姐弟呢」
「你和千鳥也談過了嗎？」
「嗯。雖然那孩子也成為了魔法少女是有點出乎意料呢。又看不出有好戰性。嘛，如果在政府出了什麼問題，我們也會支援的，所以不用擔心喲」

鈴城笑了一會兒後，把折疊的紙遞到鶫面前。鶫一邊接過那張紙，一邊歪著頭。

「這是什麼？」
「人家和百合醬的個人聯絡方式～。不管怎麼說，得到幫助也是事實嘛。──如果鶫君有什麼事的話，下次我們會幫助你的！」

畢竟已經連任六華了，還是有點權力的呢，鈴城笑著如此說。鶫一邊交替看著手中的便條與兩人，一邊用顫抖的聲音開了口。

「這麼重要的東西，我這樣的人可以收下嗎？」

六華的私人聯絡方式，到底有多大的價值連想像也做不到。
按理來說，她們是鶫一生都牽扯不上的雲頂之人。只是協助了一次，可沒想到會被如此關心。

「不用想得那麼嚴重吧？雖說是偶然，但那容貌的話似乎會被捲入各種麻煩之中嘛。而且也有千鳥的事。想作是萬一之時有個靠山就可以囉。──好啦！」

壬生一邊輕輕地拍打著鶫的背後一邊這樣說完，就把一直空著的右手伸到了鶫的面前。鶫不明所以地望著壬生的臉後，她就感覺無奈似的笑了。

「我們給了你聯絡方式。我覺得你也寫下來作交換才合禮儀吧」
「⋯⋯有必要嗎？」
「當然有必要！一起拚命戰鬥過了。已經等於是朋友了吧？」

鶫陷入困惑的同時這樣問道，壬生就用明朗的表情作出了回答。
回想起來，她從進入這房間開始，距離就莫名地靠近。或許是因為，她一開始就認為跟鶫是親密的關係──朋友。

一瞥的往鈴城的方向一看，「正是如此！」她仿彿在這樣說的點頭回應。看來似乎無路可逃。

鶫在桌上的記事本上寫下自己的聯絡方式後，輕輕地交給了兩人。總覺得，有點難為情。

兩人把收到的便條小心翼翼地收好後，拿出手機確認了時間。

「好了，事情也辦完了，差不多得走了。」
「因為傍晚有記者招待會呢。雖說是每次都有的事，但依舊很麻煩」

如此說著，兩人就憂郁地嘆了一口氣。所謂記者招待會，恐怕就是六華的就職儀式吧。雖然據說會在政府官邸大張旗鼓地進行，不過，鶫因為沒有看過直播所以不知道詳細的情況。

「這麼說來，兩人是再次當選六華了呢。恭喜」

鶫如此祝賀後，鈴城有些不滿地聳了聳肩。

「雖然排名是輸給小雪了呢。啊～，有點受打擊」
「是嗎？我倒是不介意」
「這關乎我的個人感性啊！」

兩個人一邊這樣說著，一邊慢慢地站起身。

「那麼回頭見，鶫君。之後要好好聯絡我喔」
「過一陣子再見吧！」

對於那兩人的話語，鶫爽朗地微笑了。──真的，是像朋友一樣的對話啊。

「嗯。──再見」

鶫輕輕地揮著手說道。雖然不知道是否真的還會再見面，但是關係就此結束的話，還是會覺得有點可惜。
因為是六華啦，因為是可愛的女孩子啦，並不是因為那麼輕率的理由。──是因為，她們是值得尊敬的人。她們率直的性格，對鶫來說比什麼都耀眼。

就這樣一邊目送著兩人快步走出病房，鶫一邊「呵呵」的輕輕地笑了。

──簡直，就像是暴風雨一樣呢。

自己是被捲入了同樣的事故的受害者，並肩與魔獸作戰了的『戰友』。雖然覺得對鶫來說是過高的評價，但是感覺並不壞。

鶫一邊躺在床上，一邊呆呆地盯著寫有聯絡方式的便條。特徵性的渾圓文字和稍微向右上揚的粗野文字。哪邊都是表現出個性的筆跡。

「朋友，嗎？」

──被人當面這麼說，上次是什麼時候的事呢。只是回想起來，就會感到害羞和令胸中溫暖起來似的喜悅。

「⋯⋯但是，可絕對不能對別人說呢」

同班同學就不用說了，對作為好朋友的行貴也不能說。可不知道會被說些什麼。雖說芽吹的話說出來應該也沒什麼問題，但也沒有必要胡亂吹噓吧。被問到的話再回答就可以了。
一邊考慮著這樣的事，鶫一邊看向時鐘。時間差五分鐘就到二時正。鶫確認後，像彈起似的挺起上身。

「糟糕。──有約好了去虎杖那裡的」

──今天早上，朝倉告訴了鶫 虎杖的情況。她腳腕扭傷得很厲害，似乎被吩咐出院後在家休養一周。

出院時間和鶫一樣是今天的傍晚。因為她本人很想見鶫，所以朝倉拜訪鶫「希望你在那之前去見她」

然後鶫被允許會面的時間是，今天的二時正。──也就是說沒有時間了。
鶫急忙下了床，把手裡拿著的便條夾在筆記本裡，快步離開了自己的病房。

──面對過魔獸的年幼少女，現在在想些什麼呢。鶫，對此一無所知。