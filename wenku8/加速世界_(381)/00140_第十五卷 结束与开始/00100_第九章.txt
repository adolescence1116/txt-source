「嗚啊啊……啊啊啊啊————！」

春雪揮著雙拳之餘，發出幾乎連喉嚨都要扯破的嘶吼。

「魔王徵收令」是「掠奪者」Dusk　Taker唯一的必殺技，卻遠比春雪所知道的所有必殺技都更加令人毛骨悚然。因為這一招的效果，就是半永久性地搶走其他超頻連線者的特殊能力、必殺技或強化外裝。

儘管現在想起實在太遲，但春雪現在才恍然大悟，懂得了在第二次對戰中，CerberusⅡ發動「技能捕食」之際所說的那句話是什麼意思。「你放心吧，我和他不一樣，我的能力不是『掠奪』。」——當時CerberusⅡ是這麼說的，而所謂的「他」，指的就是CerberusⅢ，而「掠奪」指的就是「魔王徵收令」。

「住、手、啊——！」

春雪雙拳在頭上交握，卯足全力砸向灰色的半透明屏障。但Black　Vice的心念「八面隔絕」以猶如隔絕了空間本身的絕對強度彈回了春雪的拳頭，反而讓他手上的銀色裝甲微微龜裂。

這時從仁子身上逆流到CerberusⅢ身上的紫色光芒有了一波特別大的脈動。一個發出耀眼光芒的球體從仁子身上被吸出來，被CerberusⅢ右肩的嘴吞了下去。多半就是構成仁子的力量——強化外裝「無敵號」的火力零件之一，終於被搶走了。

春雪驚愕地瞪大眼睛，涌起更大的震驚與戰慄，低聲驚呼。

從仁子身上流出的光芒尚未消失。這種光芒發出黏稠的聲響，貪婪地試圖繼續吸收力量。

「啊……啊啊啊……！」

春雪的驚愕化為令他頭昏眼花的憎恨與激憤，又轉變為絕望，讓他再次嘶吼。說話聲音明明傳得進八面體之中，但陶醉在掠奪快感當中的能美是不用說了，就連默默看著整個現象的Vice與Argon連頭也不動一下。

春雪再度舉起龜裂的拳頭，但就在這時，一個尖銳的嗓音撼動了他全身。

「小春，你冷靜點！」

同時右手手腕被人抓住。回頭一看，看到Lime　Bell發出堅毅光芒的鏡頭眼就近在眼前。

「不可以自暴自棄！要好好想……你一定可以想出方法救仁子！」

「可是……可是、可是！」

春雪大聲嚷著，就要用蠻力揮開她的手，就在這時——

他覺得收疊在背上的新翅膀微微震動了一下，就像是在斥責他。

…………沒錯。愈是這種時候……愈應該冷靜下來……擴大視野。

…………要觀視一切……思考……該做的事。

春雪拚命透過想像，將貫穿全身的發作性激憤濃縮成小小的球體，沉入意識底層。現在需要的不是憤怒。有能量拿去憤怒，還不如盡可能加快、加深思考。

當春雪好不容易找回了冷靜，就輕輕放下被千百合抓著不放的右手，說道：

「……知道了，等我一下就好。」

他動用所有感覺，觀察聳立在眼前的巨大正八面體。

哪怕是由實力深不可測的Black　Vice所使出的心念，應該還是沒有綠之王Green　Grandee的心念「光年長城」那種絕對壓倒性的強度。這個八面體是由本來組成手腳的薄板所構成的集合體，所以接合處的強度應該會比平面的部分更差吧？如果真是這樣，那麼該攻擊的——

「……不是面，是角！」

聽到春雪的呼喊，立刻有了反應的tIPard小姐。

「交給我。」

她才剛回答完，就張大了嘴跳起，用心念的利牙咬在八面體上的一個頂點。這次牙齒並未被彈開，四根牙齒勉強咬住了四個面。她綳緊野獸身軀上的每一條肌肉，產生莫大的咬合力，咬得整個屏障都受力彎折。

春雪有了破得了屏障的確信，然而……

八面體突然往右迅速轉動了四分之一圈，就在深深陷進地面的下半部鑽得大理石石屑四濺的同時，甩開了Pard小姐以微妙角度勉強固定住的牙齒。

Pard小姐下顎鏗一聲咬合，整個人被甩得飛起，但才剛落地又撲了過去，試圖咬向另一個角。但這次八面體往左旋轉，又讓她沒能咬中。

「阿拓，小百！」

春雪大喊一聲，為了固定住八面體而雙手按住其中一個面。拓武與千百合也跑到另一頭，站穩腳步按住。然而平面上沒有任何東西可以抓，八面體第三次做出旋轉，把他們三人都甩了開去。

「嗚…………」

就在春雪咬緊牙關之際……

第二顆光球從仁子身上被吸了出來，被CerberusⅢ的右肩吞食掉。

再這樣下去，構成「無敵號」的零件——根據春雪的推測，共有主砲、飛彈發射器、加裝機關砲的駕駛艙、背面推進器、腳部等五個零件——全都會被搶走。春雪拚命壓抑不斷高漲的焦躁，繼續思考。

八面體的頂點就是弱點，這點已經無庸置疑。但要攻擊頂點，就非得想辦法阻止八面體旋轉不可。從四個方向同時攻擊四個頂點？不對，就算這樣也無法阻止旋轉。正八面體是以下端的頂點做為旋轉的之點，但這個頂點深深埋進大理石地面，連碰都碰不著。

…………陷進，地面……

「————！」

春雪眼睛瞪得老大，仰望上空。

六個頂點之中，真正可以說是弱點的，就只有字面意思上的頂點——也就是最頂端的一個點。即使八面體可以水平旋轉，應該也無法垂直轉動，因為下端的頂點已經牢牢固定在地面。但即使想攻擊頂點，靠Pard小姐的牙齒攻擊，還是會被水平旋轉甩開。必須以分毫不差的准度，從正下方往正下方的垂直方向施加壓力。

一想到這裡，幾十分鐘前才聽到的一句話就在春雪腦海中閃過。他猛力轉過身去，跟說這句話的人問清楚。

「……阿拓！你必殺技計量表呢？」

Cyan　Pile什麼都沒問，立刻回答：

「還是滿的！」

「好，我把你帶到八面體正上方，麻煩你往正下方用那招！」

只說這句話，拓武似乎就聽懂了春雪的意圖。面罩上成排縫隙下的鏡頭眼一瞬間瞪大，接著立刻點頭答應：

「知道了，包在我身上！」

春雪從後抱起拓武，張開背上的銀翼並猛力振動。他一瞬間就抵達了聳立到上空二十五公尺高度的正八面體正上方，俯瞰這個在中庭裡切下一塊方形區域的心念屏障。

正好就在這時，第三個光球離開仁子身上，被CerberusⅢ的肩膀吞沒。

還剩下兩個。一旦所有強化外裝都被搶走，仁子就會失去被譽為「不動要塞」的壓倒性火力。

春雪揮開一瞬間的恐懼，進入攻擊態勢。

首先春雪牢牢固定住拓武的位置，同時將身體倒成水平方向。同時拓武將心念劍變回打樁機，將從砲口露出的鐵樁對準了八面體的頂點。

「上吧，小春！」

「阿拓，給他轟下去！」

春雪為了因應招式的反作用力，將翅膀完全張開，緊接著……

「螺旋……重力……錘——！」

籠罩在藍色光芒中的打樁機砲口唰的一聲擴大，先前收納回去的鐵椿變成巨大的電動錘鑽，往後噴出火焰發射出去。

這根前端變平的鋼柱猛烈旋轉，精確地捕捉到了正八面體的頂點。

一陣像是壓縮了四周所有空氣似的震天巨響。構成心念屏障的八片半透明玻璃滋滋作響，從衝擊點迸出的大量火花就像瀑布似的從傾斜的平面上流下。

到了這個時候，維持八面體的Black　Vice才終於抬頭看了春雪與拓武一眼。他輕輕歪了歪沒有臉的頭，巨大的八面體彷佛就被這個動作觸發，開始往反時針方向旋轉。由於旋轉方向與Cyan　Pile的電鑽相反，從衝擊點產生的火花與巨響也都當場加倍，強烈的振動甚至傳到了抱住拓武的春雪身上。

只要電動錘鑽的射出角度從垂直方向偏離一度，相信都會無法捕捉到高速旋轉的八面體頂點而滑開，讓他和春雪一起摔落到地上。

但Cyan　　Pile的3級必殺技「螺旋重力錘」有著只能往正下方發射的限制。換個角度來看，也就表示不用自己辛辛苦苦調整角度，也能夠將發射角度固定在垂直方向。

「唔……哦哦哦哦————！」

拓武大聲吼叫，全身迸射出藍色的過剩光。心念的光芒從右手灌進電動錘鑽，將灰色的鋼鐵變成超高硬度的剛玉。噴出的大量火花漩渦和藍色的鬥氣交雜，將中庭照得光彩奪目。

一聲從未聽過的異樣彎折聲撼動了大氣。正八面體似乎承受不了壓力，轉速慢慢降低，最後終於停任。相較之下，化為藍寶石的電動錘鑽則以遠超越當初將春雪從屋頂一口氣打到一樓時的出力，持續壓迫八面體——

第二次的彎折聲，伴隨著尖銳的材質哀嚎聲。細微的裂痕就像閃電，從正八面體頂點竄過往下延伸的四個邊。但龜裂只進行到下一個頂點就停住，並未造成整個八面體的瓦解。

「就只差……那麼……一點了……！」

聽到拓武難受的聲音，春雪下定決心大喊：

「我也來幫忙！」

春雪抱住拓武的雙手加上了更多力道，讓兩個虛擬角色合而為一。除了Silver　Crow本來的銀翼之外，更伸展出他剛被授與的全新白色翅膀——「梅丹佐之翼」。春雪往四片翅膀呈X字形張開的翅膀卯足意志力，大喊：

「給我……碎掉啊————！」

一股像是巨大火箭噴射的白光垂直上衝，產生出莫大的推力，透過春雪與拓武的身體，再透過電動錘鑽，灌注到八面體上，壓得八個面大幅彎折。

一度停止的龜裂沿著側面的頂點往下延伸，頂點處更往左右裂開，和其他頂點竄出的裂痕相連。

就在每一邊都竄過裂痕的瞬間，一聲格外高亢的破壞聲響貫穿了整個空間。

霧黑色的屏障化為無數碎片，在夕陽中閃閃發光地碎裂四散。

同時CerberusⅢ搶走了第四個光球。春雪與拓武仍然牢牢抱在一起，挺著繼續發出轟然巨響轉動的電鑽，順勢衝向正下方的能美。

「「唔喔喔喔喔喔喔喔喔————！」」

灌注了兩人份力量與意志的螺旋重力錘，將行進軌道上紛紛飄舞的屏障碎片化為微小的粒子一路往下挺進，眼看就要鑽上CerberusⅢ的面罩——

卻在最後關頭被左前方的Argon　Array發射的四道雷射阻礙。其中兩道由春雪勉強用「光學傳導」特殊能力擋開，但剩下兩道則擦過拓武的側腹部與左肩，讓他失去平衡。電動錘鑽因而失去準頭，旋轉的打擊面因而重重撞在能美左側五十公分的位置，將大理石地磚撞得粉碎。

就在春雪以翅膀煞車，以便使拓武不受損傷著地的瞬間，他覺得聽到了好友說話的聲音。

——小春！我不要緊，趁現在去救紅之王！

——了解！

就在剎那間的對話結束的同時，春雪放開雙手，轉身面向釘住仁子的祭壇全力飛翔。左側的Argon四個鏡頭再度發出紫色的光，但Pard小姐在灑落的碎片下，整個人撲了上去，導致雷射射偏，徒勞無功地穿進後方的校舍。

「仁子————！」

春雪呼喊她的名字，張開雙手牢牢抱住被釘在漆黑十字架上的深紅色虛擬角色。同時他以雙手試圖破壞十字架，但Vice似乎想避免在失去右手之後跟著又失去左手，只見十字架變回多片薄板沉入地面。

如果窮追下去，也許能夠破壞其中幾片薄板，但現在有更優先的事要做。

「喔喔喔！」

春雪大喝一聲，以籠罩心念銀光的右手劃過，斬斷了連接CerberusⅢ與仁子之間的紫色連線。就在這一瞬間，正要從仁子身上被抽出的第五個光球當場靜止不動，慢慢地回到虛擬角色身上。

————仁子！

春雪的第二次呼喊並未出聲，而是在多種情緒洶涌翻騰的心中，呼喊這位重要的朋友。

嬌小圓潤的對戰虛擬角色，確實存在於他的懷裡。從她在中城大樓被Black　Vice綁走到現在，所花的時間約為四十分鐘。看似短暫，但春雪卻覺得已經過了好幾天。

而且被搶走的事物也極為重大。

CerberusⅢ——能美以「魔王徵收令」從仁子身上搶走的強化外裝多達四個。雖然不知道是哪四個零件，但簡單換算下來，已經高達「無敵號」的八成。從過去的案例來思考，相信一旦Vice和Argon判斷情勢不利，立刻就會試圖帶著Cerberus逃走。無論如何他都得在這之前搶回四件強化外裝。

……仁子，你等著。我馬上就把你寶貴的……

就在春雪想到這裡時，他臉部下方距離只有幾公分的小小面罩微微一動，變黑的鏡頭眼也開始發出淡淡的綠光。

會是她擺脫了Vice的十字架，因而恢復意識了嗎？春雪想到這裡，就要朝懷裡的虛擬角色輕聲呼喊，然而……

他連「仁子」的「仁」字都尚未喊出，就發生了一個出乎意料之外的現象。

Scarlet　Rain嬌小的虛擬角色身上強烈放射出小恒星似的火紅過剩光。這股蘊含這強烈熱量的衝擊波，震開了春雪抱住仁子的雙手，讓他呈大字形往地面墜落。儘管勉強避免一屁股坐倒在地的窘態，卻變成了要蹲不蹲的尷尬姿勢，抬頭看著飄浮在祭壇上空的虛擬角色。

仁子在熱氣形成的上升氣流中緩緩下降，一對鏡頭眼依序捕捉到離她不遠處的春雪、將打樁機對準能美的拓武、在稍遠處以聖歌搖鈴擺好應戰架式的千百合，以及與Argon對峙的Pard小姐。春雪覺得她的眼神在瞬間稍微變得溫和，但也只維持到她轉而瞪視敵方的三人為止。

圓滾滾的鏡頭眼從原本的綠色，轉變為令人想起超高溫火焰的泛青色。全身溢出的火焰鬥氣變得更旺，加熱黃昏空間裡冰冷的空氣，引發了搖晃的蜃景現象。

她乘著強烈的熱氣，以充滿破力的低音說出第一句話：

「你們這些家伏……還真是為所欲為……」

這時仁子緩慢的降落正好結束，降落在小小的方形祭壇上，雙手抱在胸前說道：

「我告訴你們，這筆帳可不是加倍奉還就能了事。我要十倍……不對，加上我的朋友也受你們照顧，我要五十倍奉還，把你們烤得連焦炭都不留，給我覺悟吧。」

…………是仁子。

春雪搖搖晃晃地站起，同時感受到胸中一股熱流上衝。

這才是外號「血腥風暴」、「不動要塞」的Scarlet　Rain。哪怕被迫陷入接近零化的狀態四十分鐘，哪怕強化外裝遭人搶走，第二代紅之王靈魂中的火焰仍未消失。

春雪知道現實世界中的仁子，是個有時會說出喪氣話、讓人看到她流眼淚的十二歲小女生。說不定那才是仁子平常的面貌。然而如果一個人處在絕境之中卻並未屈膝死心，能夠握拳站起，那就是……不對，那才是真正的堅強。

這才是甚至超越在BRAIN　BURST系統之上的，真正的心念。

Pard小姐本來正與Argon對峙，這時弓起豹的身體高聲吼叫，跳起一大步，守在仁子腳邊。春雪也走上幾步，在祭壇右側備戰。拓武與千百合也迅速來到左側排好。

面對以Scarlet　Rain為中心組成隊形的五人，最先有反應的是Argon　Array。她以護目鏡下露出的嘴角露出淺淺的微笑，以兼有開朗與冰冷的嗓音說：

「小不點，你挺囂張的嘛。被搶走了足足四件強化外裝還這麼有氣勢，真了不起。換做是我，光是這頂帽子被搶走，我馬上就會哭得超傷心了呢。」

「……那我就如你所願，把你的帽子連著那煩人的外撇髮型一起扯下來，讓你哭個夠吧。」

仁子說話的內容與口氣，都讓人感覺不出她對現況有任何困惑。相信一定是在被Vice強迫陷入零化狀態時，意識仍然並未消失。

仁子的舌鋒毫不退讓，讓分析者笑得雙肩晃動。

「啊哈哈，好可怕喔。可是我好歹也是個女孩子，不想被剝得頭上光禿禿的說。而且好久沒有做這種像是在戰鬥的事情，可真是累壞了我。剩下的就交給年輕人，我可要去隔岸觀火了……所以啦，小三，就拜託你啦。畢竟你也得償所願，拿到了新的玩具。」

聽Argon這麼說，春雪將視線移到CerberusⅢ身上。

這個灰色的金屬色虛擬角色，從被拓武與春雪打斷「魔王微收令」之後，已經保持沉默將近兩分鐘之久。他雙手軟軟垂下，頭也深深低下不動，就好像是一具關掉了電源的機器人。

——不，也許真的是這樣？會不會是一口氣搶走了多達四件大型強化外裝的反作用力，引發容量超載的現象，因而不能動彈……？

以前能美就曾在搶走春雪的飛行能力後，又想連拓武的打樁機也搶走，結果卻搶不到。春雪回想當時的情形，想到了這個可能。

但緊接著就聽到垂下的面罩下流露出毒辣的竊笑聲，否定了春雪這帶有几分期待的推測。

「哼……哼，哼，哼哼哼哼……雖然在最後關頭有人來礙事，實在讓我很火大……不過這可了不起……只能說真不愧是『王』的力量啊……以前我從一些無名小卒身上搶來的那種的刀啦、觸手啦，寒酸的翅膀啦，跟這比起來簡直是垃圾。畢竟我只搶走四件，就把三人份的容量都榨乾了啊……」

能美說出這番仿彿看穿了春雪心思的台詞後，慢慢抬起頭來。他臉部的護目鏡仍然完全咬合，但春雪卻看到了一種幻覺，覺得裡頭有一對鏡頭眼發出深紫色的光芒。

能美慢慢仰起上身，朝上舉起伸出鈎爪的雙手，突然加大音量大喊：

「這就是掠奪的快感！別人拚命努力才得到的力量，花了長年心血才培養出來的力量，一瞬間就變成我的……哼，哼哼，我會用這種力量，搶走更多更多的東西……哼哼哼哼，哼哈哈哈……哈哈哈哈，啊哈哈哈哈哈哈！」

能美的大笑聲，與兩個月前與春雪等人展開激戰的那個真的Dusk　Taker如出一轍。不，如今甚至可以說，這個在他們眼前笑個不停的金屬色虛擬角色才是真貨。因為從現實世紀的能美征二身上被切除下來的「惡」，被一個至今仍未現身，有著更巨大惡意的人召喚出來，這才是CerberusⅢ的本質。

他們萬萬不能容許這種事。非得將能美的記憶——不，應該說是他的亡靈——重新埋葬到BRAIN　BURST中央伺服器內不可……如果可以，更必須加以完全消滅。

而現在這一瞬間，也許就有可能辦到。

哪怕人格改變，系統上所蓄積的點數應該也不會有所不同。CerberusⅠ在即將切換到Ⅲ之前就說過，說他的點數只剩下10點。也就是說，一旦被同樣5級的春雪打倒，就會剛好有10點點數從Wolfram　Cerberus身上轉移出去，導致他喪失所有點數，就此完全消滅。

以這個情形而言，CerberusⅠ的記憶應該會進行正常的刪除（或是移動）處理，但Ⅱ與Ⅲ的記憶會如何處理就不知道了。也許會再度被收回伺服器內，也說不定這次真的會完全消失。

但即便能夠消除能美的記憶，屆時CerberusⅠ也會跟著消失。

儘管沒有確切證據，但他多半是根據奠基於「心傷殻理論」而訂立的「人造金屬色計劃」所創造出來的悲慘超頻連線者。他在Argon的命令下，為了賺取點數而被迫維持在—級持續對戰，卻並未失去一顆坦率、認真與熱愛對戰的心，不但是個罕見的天才，同時也是春雪重要的朋友。

即使是出於當事人自願，春雪還是不想讓他喪失所有點數。春雪希望能將他從所有恩怨糾葛中解放出來，跟他再打一場又一場的對戰。

春雪心中兩股相反的情緒天人交戰，仁子則對搶定她強化外裝的對手拋出尖銳的話語：

「……原來如此，你這小子果然和傳聞中一樣個性扭曲。像你這樣的傢伙根本沒辦法把『無敵號』駕馭自如，因為強化外裝也是有心的。」

「哈，哈哈哈哈！」

能美再度短短地笑了幾聲，大動作攤開雙手。

「這的確像是喜歡自稱什麼超頻連線者的傢伙會說的話啊！那我就證明給你們看吧……我會證明所謂的心這種東西，不管在加速世界，還是現實世界，都沒有任何力量！除了對我的忠心以外！」

他左手迅速閃動，操作系統選單。春雪屏氣凝神之餘，想起能美的確很討厭喊語音指令。

尖銳的食指迅速地連續敲下四個別人看不見的按鈕。

一陣轟然巨響響起，撼動中庭的地面。

CerberusⅢ的四周出現了好幾個半透明的巨大立方體。這些立方體迅速增加細節與質感，讓覆蓋在紫色裝甲板內的武裝物件群化為實體。

首先由細長的駕駛艙從後方籠罩住CerberusⅢ的身體，左右方再接上與粗大雷射砲化為一體的雙手，背後再接上有著四個大型噴嘴的推進器，下方也伸出一雙雄健的腳接了上去。

春雪等人並非默不吭聲地旁觀這合體場面。仁子與春雪有著遠程攻擊用的心念攻擊，剛看到強化外裝開始物件化，立刻就分別讓雙手籠罩深紅色與銀色的過剩光，但Argon與Vice在能美後方做出同樣的動作，讓他們沒有機會出手。

就在陷入膠著狀態的雙方陣營之間，四件強化外裝發出一陣格外強烈的閃光與巨響，和CerberusⅢ合體完畢。

物件的形狀與色彩，都和仁子本來的「無敵號」相去甚遠。由於少了一個零件——能美沒能搶到的多半是飛彈發射器——份量感是比不上原版，但形狀比較接近人體而非要塞，變得比較細長，所以高度直逼四周的校舍。

就如他身上那介於遠程與近戰中間的深紫色裝甲所示，裝備在雙手外側的雷射砲雖然規模變小，卻多了由四根尖銳鈎爪構成的手掌部分。雙腳腳尖也各伸出兩根長爪，肩上與膝上也都配備了巨大的尖刺，給人的整體印象已經不只是巨人，反而更接近惡魔。

CerberusⅢ幾乎完全被厚實的駕駛艙罩住，高高舉起強化外裝的雙手，以增幅過的金屬質感失真嗓音嘶吼：

「怎麼樣……這才叫作力量！這才叫作支配！這才是唯一絕對不可動搖的力量！哈哈哈哈……哈哈哈哈哈哈哈哈哈！」

這幾句話與先前真正的能美大呼痛快時所說的話一字不差。這個事實讓春雪深深意識到，眼前的「能美」只不過是從抽出的複製記憶模擬出來的存在。

所以更非得除掉他不可。

這既是為了已經在現實世界重新走出一條路的能美征二，也是為了被塑造出來當成媒介，還不懂得對戰的喜悅就被迫對戰到今天的CerberusⅠ。最重要的是，這也是為了在別人的意志下，被人當幽靈叫出來利用的CerberusⅢ自己——

「……小百。」

春雪用只勉強聽得見的音量對這位兒時玩伴說：

「這次也要靠你了。時機到了我會跟你說，在這之前就請你專心保護自己。阿拓，小百就麻煩你護衛了。」

春雪確定綠色尖帽與藍色頭盔都微微一動，接著對紅之團的兩個人也說了一聲：

「仁子，Pard小姐，我們得和『無敵號』打，沒關係吧？」

「沒差，儘管放手去打。」

「K。」

她們立刻做出非常靠得住的回答，讓春雪覺得自己反而蒙她們推了一把，跟著點了點頭。

就在這個時候，紫色惡魔往前踏出了撼動大地的一步。他讓雙手鈎爪緩緩開閉，發出舔著嘴唇似的聲音說：

「『勇者和手下』的作戰會議開完了沒呀？請你們千萬別讓我失望……至少也要讓我玩個五分鐘啊！」

春雪承受著巨大的壓力擺好姿勢備戰，同時也不忘留意惡魔的後方。

「合體CerberusⅢ」多半會是駭人的強敵，但也不能忘了Argon　Array與Black　Vice的存在。Argon的戰鬥力幾乎完全無損，Vice儘管因為「八面隔絕」被破壞而失去右手右腳，但並未露出承受劇痛的跡象，若無其事地站著不動。一旦春雪等人露出任何破綻，相信他一定會毫不遲疑地用剩下的左手左腳攻擊。

——無論什麼時候，都要冷靜地觀視整個戰場。

春雪這麼告誡自己，能美則舉起左手的雷射砲，像是在對他挑釁。這直徑怕不有十五公分的漆黑砲口發出紫水晶色的光芒，充電的嗡嗡聲更是愈來愈大。

這時上背部的白色翅膀——「梅丹佐之翼」微微震動，像是在警告春雪。

……我知道，我才不會挨到那麼明顯的攻擊……

春雪本來打算在雷射即將發射之際起飛，貼到能美巨大的身軀上賞他一波連續攻擊，於是在胸中反射性地這麼回話。

但梅丹佐的警告，並不是針對合體CerberusⅢ的遠程攻擊。

「……？」

緊靠在他左邊的仁子全身一顫。

「怎麼了！」

連Argon　Array都把注意力從戰場上移開，仰望北方的天空。春雪也順著她的視線瞥了過去，登時看得目瞪口呆。

由從橙色到深藍色構成的晚霞天空背景下，一條紅線無聲無息地延伸過來。

以遠程攻擊來說未免太慢，幾乎感覺不到任何物理上的威力。即使是瞄準春雪等人，相信要閃避或擋開應該都是輕而易舉。更何況以現在的軌道而言，這道紅光多半會直接從學校上空通過。

然而——

春雪突然籠罩在一種像是被潑了一身冰水似的恐懼之中，整個虛擬身體連手指都完全僵硬，虛擬的呼吸也因而停止。同時卻又有一股只想馬上拔腿就跑的衝動，讓動彈不得的身體劇烈顫抖。

無論仁子、Pard小姐、拓武還是千百合，也都呆站在原地盯著天空看。如果能美發射雷射，相信所有人都會被轟個正著。然而已經進入主砲發射態勢的能美似乎也感覺到了異樣，仰起強化外裝巨大的身軀，從駕駛艙部位的縫隙仰望天空。

正好就在這時，來到中庭正上方的紅線以無視任何物理定律的動向彎往正下方。春雪聽到了小小的聲響。從像是風聲的咻咻聲，到像是許多人哀嚎的噪音。

「那是，什麼玩意兒——」

就在能美訝異發問的下一瞬間……

紅光扭動著命中了駕駛艙。但並未發生類似爆炸的現象，光就像軟泥似的附著在裝甲表面，從縫隙鑽進內部。

「嗚……嗚哇啊！住手……！我可沒聽說會有這種事啊……Vice！Argon！趕快阻止這東西啊——！」

能美發出哀嚎似的嘶吼。強化外裝的雙手亂揮一通，雙腳踏得中庭地磚殘破不堪。雖然被裝甲遮住而看不見，但駕駛艙內側肯定發生了某種駭人的現象。一種筆墨難以形容的可怕現象。

Argon迅速拉開與胡亂掙扎的合體CerberusⅢ之間的距離，難得露出驚愕的模樣驚呼：

「不會吧……再怎麼說也未免太快了吧！難道說……那些傢伙幹掉了那個……這多半連社長都沒想到吧…………」

春雪一時間聽不懂她這番話的意思，但看來這個情形連加速研究社都並未料到。

「哇啊啊啊啊啊——這些傢伙……鑽到我體內……住手！不要啊啊啊啊——！」

紫色惡魔高聲哀嚎，猛力撞上南側的校舍，接著又像失控似的舉起雙手，開始用蠻力毆打校舍三樓部分。由於整棟建築物都有著玩家住宅屬性，連一面玻璃窗都並未破損，但強烈的衝擊撼動了地面，搖動春雪等人的身體。

這個刺激讓春雪好不容易擺脫呆滯的狀態，但仍無法判斷該如何行動。

這時喊話的是站在他左邊的仁子。

「雖然不知道發生了什麼事……不過我們日珥的作風就是這種時候先轟一波再說！Crow，我們動手！」

「了……了了了解！」

春雪握緊拳頭來揮開恐懼與震驚，讓雙手籠罩在銀色過剩光中。

仁子也一樣把拳頭籠罩在一層紅色的鬥氣中，擺好拳擊架式。

「——『雷射標槍』！」

春雪右手射出白銀的標槍。

「——『輻射連拳』！」

仁子右手連續發射出將近十發火焰拳。

兩人的心念攻擊命中了猛力掙扎的合體CerberusⅢ左肩靠胸口部位，引發了大規模的爆炸。龐大的身軀一歪，關節部分遭到破壞的左手慢慢分離，噴出瀑布股的火花滑落到地面。

紫色惡魔踉蹌了幾步才站穩，接著停下了動作。這陣毆打堅不可摧的牆壁所造成的噪音平息後，能美先前被掩蓋住的低呼聲就像詛咒似的回蕩在中庭。

「……你們……騙了我……說什麼會給我新的力量，讓我報仇……話說得好聽……你們從一開始，就打算，這麼做…………」

對此Argon　Array做出的回答，多半已經表達出對她而言最大的歉意，但還是帶點輕浮。

「對不起喔，小三。其實呢，本來我們還可以讓你多玩一陣子。可是啊，你也知道，我們一直只靠最低限度的人數在撐，計劃有時候就是會跟不上變化嘛。」

「少……廢話。快點……解開這個，救我出去……不然，連你們我也……」

說著能美舉起巨大的右手，以雷射砲瞄準Argon與Vice。但兩名8級玩家不為所動，不約而同地聳聳肩膀，這次換Vice回答：

「這可傷腦筋了。Taker同學，在這種狀況下要救你，再怎麼說都太難了啊。」

聽到這句耳熟能詳的台詞，能美的嗓音中蘊含了更深的怒氣。

「你又要……又要放棄我嗎？Vice……竟然兩次……放棄我……」

「Taker同學，你放心吧，我想應該不會弄成有二就有三。」

Black　Vice說完風涼話，將只由並排薄板構成的頭部轉朝向春雪等人：

「最後我就對黑之團以及紅之團的各位給個忠告。建議各位最好別想搶回強化外裝，立刻脫身。雖說融合來得太早，但那玩意兒已經不是你們應付得了的東西了。」

「你這傢伙，想跑嗎！」

仁子尖銳地指出這一點，失去一隻手一隻腳的積層虛擬角色若無其事地點點頭說：

「那當然，畢竟我和Argon乜都很愛惜自己的性命。雖然作戰目標的達成率頂多只有四成，不過我們就別太貪心了。」

「就是這麼回事。如果你們包能順利跑掉，到時候我們再來玩玩羅。小貓咪，跟你聊天很開心。」

Argon輕輕揮動右手的同時，構成Vice身體的薄板低溜溜轉動，轉眼間就融合成兩片大型的板子。春雪驚覺地朝他們兩人腳下一看，發現他們的腳剛好碰到西南方校舍的影子。

「嗚……！」

春雪咬緊牙關，但當下的最重要事項並不是追擊Vice他們，而是要搶回仁子的強化外裝，所有人一起與待在中城大樓的黑雪公主等人會合。要達成這個目的，就非得破壞紫色惡魔，把CerberusⅢ從駕駛艙拖出來不可。

就在兩片薄板包夾住Argon的瞬間，能美發出了充滿怒氣的吼聲：

「Vi……ce————————！」

他從右手的雷射砲，射出毒艷的紫色雷射。

但這時漆黑薄板已經融合成一片，垂直沒入地面的影子之中。緊接著雷射命中，竄起一道衝天的火柱。無數大理石地磚剝落飛起，但其中看不見Vice與Argon的身影，相信他們早已潛入校舍內部的影子之中遞走了。

「該死！該死！該死啊啊啊啊啊啊啊啊！」

能美在駕駛艙內散播龜裂的怒氣。

「我不承認！我不准這樣的事情發生！來人啊，是誰都好，來這裡……把我……把我……啊，啊啊……啊啊啊……住手，不要，我不想失去……這是我的力量……是我的……」

充滿詛咒的呼聲漸漸減弱，但紫色裝甲表層滲出稀薄影子似的鬥氣卻與呼聲成反比，變得愈來愈濃。

「……Crow，再來一次！」

在仁子尖銳的呼喝聲促使下，春雪半自動地舉起了右手。他揮開胸中涌起的恐懼，專心凝聚心念。

再度發射出去的「雷射標槍」與「輻射連拳」，在呆呆站著不動的合體CerberusⅢ背面打個正著——本來應該是這樣，然而……

在裝甲表層竄來竄去的影子鬥氣彷佛有著自己的意志，匯集起來形成厚實的裝甲，擋下了兩種心念攻擊。

「什麼……」

「沒……沒損血……？」

不只是仁子與春雪，連拓武等人也不約而同發出同樣的驚呼。但駕駛艙內的能美似乎連受到攻擊這件事都並未注意到，繼續低聲呼喊：

「我……不要……我……在慢慢，消失……什麼都，看不見……聽，不見……啊啊啊……消失……消，失……」

匆然間。

他的聲調變了。恐懼、憤怒等所有情緒都完全脫落，多了一種數位噪音般的聲質。

「消失……消失……消失、失、失、迪、迪迪迪、迪迪、迪嚕、迪嚕迪嚕迪嚕迪嚕、迪嚕迪嚕迪嚕迪——————」

異樣的哀嚎突然完全中斷。

巨大的紫色身軀仍然停在不自然的姿勢。連黃昏空間中理應會始終吹個不停的微風都停了，春雪就在失去了所有聲響的中庭裡，困在一種他從未在加速世界感受過的戰慄之中，一聲不吭地呆呆站著不動。仁子、Pard小姐、拓武、千百合也都一樣，什麼話都不說，彷佛只要說出一句話，就會揭開更可怕的事物。

打破這陣寂靜的，是一道黏稠的水聲。仔細一看，黑濁的鬥氣就像血液似的，從巨人左肩的傷口滴落。這種鬥氣牽出長長的絲線流到地上，累積到一定程度後，就化為黏泥開始爬行。這些黏泥的目標，是落在一小段距離外的左手。

也許應該攻擊黑色黏泥才對，但春雪就是動彈不得。黏泥轉眼間就爬到左手，從被破壞的關節部分鑽了進去。

尖銳的四根鈎爪忽然顫動。詁泥連接起本體與左手，原本拉得細長的黏泥開始收縮，把手臂拉回肩膀。春雪茫然仰望這幅光景，看著巨大的鋼鐵手臂被吊上空中，發出黏稠的聲響，和位於地上六公尺高度的左肩接合在一起。

在無限制空間中遭到破壞的強化外裝，除非透過擁有者離線再登入，否則都不會重生。

合體CerberusⅢ輕易地顛覆了這個常識，讓左手重生後，慢慢讓巨大的身軀重新站穩，往左轉過九十度，從正面和春雪等人對峙。

中央是駕駛艙，側面有著雙手，下方有著雙腳，背面有著推進器，這樣的組成讓巨人沒有頭部，但春雪確切地感受到了。感受到有充滿無底飢餓的視線從高處射在他們五個人身上。

「嚕……迪嚕嚕嚕…………」

這是一種像野獸又像機械的異樣吼聲。在巨神全身竄來竄去的陰影鬥氣急速增加密度，發出余屬質感的擠壓聲，讓裝甲開始變形。直線造型開始扭曲、彎折，形成有機質的曲面。四肢的鈎爪也變得巨大，到處都露出了鰓一般的成排縫隙。

春雪注意到不知不覺間，中庭正上方本來滿天晚霞的天空已經聚集了厚重的烏雲。每當雲層深處閃出蒼白的閃電，隨即就響起低沉的雷鳴。在光漸漸遠去的世界之中，巨人仍然持續變化成真正的惡魔。

雙肩與雙膝的尖剌伸長到將近兩倍，駕駛胎的縫隙被鱗片般的金屬板完全堵住，雙手的雷射砲變成環形動物似的模樣，背面的推進器則變成巨大的突起。

最後，駕駛艙的上方冒出了一個半球形的「頭」。

半球形的前半部就像眼瞼似的睜開，從中出現的，是有著血色虹膜的巨大眼球。這次惡魔以真正的視線射穿春雪等人，高高舉起有著鐮刀般鈎爪的雙手，以驚天動地的音量發出咆哮。

「迪嚕嚕……嚕嚕羅嚕羅喔喔喔喔——！」

烏雲接連竄出紫色的雷光，打在惡魔的四周。

這個屹立在中庭的形體，既不是「無敵號」，也不是「合體CerberusⅢ」。

儘管大小不一樣，但春雪曾經目擊過極為相似的形體。一次是在過去的影片記錄畫面中，一次則是在禁城所作的夢中。

另外Silver　Crow自己也成變成這種模樣……

春雪腦中浮現出三天前他與黑雪公主及冰見晶談話中說過的一個字眼。他以結冰般的戰慄與恐懼，為不由自主說出的這個名稱賦予了色彩。

「……災禍之鎧……MkⅡ…………」

（待績）
