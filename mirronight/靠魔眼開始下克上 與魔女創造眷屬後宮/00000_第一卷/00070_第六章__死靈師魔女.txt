第六章  死靈師魔女

「為什麽我會在這裏？那還用得着問嗎」
回答問題的芭妮拉的眼睛，在艾特看來仿佛失去了光澤壹樣。
「為了我能先和古魔女契約，得到王之力啊」
壹邊這麽說着，芭妮拉丢出了圓環。
即便如此，艾特也紋絲不動。
因為從他所在的位置，能看出圓環打得很偏。
和他預測的壹樣，圓環朝着其他地方飛去。
緊接着，背後傳來了慘叫。
那是追趕着艾特的兩名帝國兵發出來的。
回首看去，他們和盧法斯壹樣倒在地上，脖子被圓環給切開了。
芭妮拉接住了飛回來的圓環。
此時剩下的兩名帝國兵出現在艾特他們面前，朝芭妮拉舉起了槍。
「你這混搭！」
「竟敢把盧法斯殿下給！」
「你們都給我退下！」
低聲沖着帝國兵們壹喝之後。
艾特瞪向芭妮拉。
「我沒問你這個。我是在問，為什麽應該在照料梅洛蒂的你會出現在這裏」
「啊啊，你問這個啊。我是偷偷跟在你們後面的。從你們離開之後，就壹路在跟着」
「......梅洛蒂怎麽了」
「不清楚呢？應該還在睡吧？要是醒了應該會吓壹跳吧。畢竟周圍沒壹個人在，只剩她自己了」
「你不是答應我了嗎。會照看梅洛蒂」
「可是，在毀約這點咱們彼此彼此吧。壹個人去探索，搶在我前面和古魔女契約的可是艾特你」
「——他們，都死了」
莉莉希娅的呢喃，傳了過來。
她蹲在地上，查看了盧法斯等人的情況。
似乎是确認他們已經斷氣了。
「為什麽，你要這麽做......」
莉莉希娅起身問道。
至于她的表情，能看得出是憤怒。
「為什麽，他們不是莉莉希娅你的仇人嗎？我才要問你為什麽露出這種表情。我幫你解決了他們，你不應該高興才對嗎」
「要你管！我還有很多事想問他們呢！而且，你不是說這次會把古魔女——把王之力讓給我嗎！那你為什麽還要做這種事......」
「那個啊，當然是騙你的啦」
芭妮拉滿不在乎地答道。
「我怎麽可能把東西讓給别人呢？自打得知艾特和古魔女契約之後，我就壹直在盤算我絕對也要搶在你們前面，先進行契約」
「既然你這麽想要，直說不就完了。直說的話——」
「你就會讓給我？」
「這......」
「對吧？你沒理由讓我優先。所以，我才會這麽做。無論如何，不擇手段也要把想要的東西搞到手。這就是本小姐，芭妮拉的做派」
芭妮拉咧嘴壹笑。
「我沒有古魔女的力量，也不是眷屬，獨自壹人是軟弱無力的，我知道只靠自己絕對到不了這裏，所以，才利用了你們」
「然後呢，你接下來想怎麽辦。打算就這樣和我們訣别嗎？」
「不，并不是」
芭妮拉的嘴角咧地更高，
「我要用獲得的王之力，得到壹切想要的東西。為此——」


「你們得死在這裏」


應該是和古魔女的契約，強化了身體能力的緣故吧。芭妮拉以難以想象是她會有的速度，拉近了和莉莉希娅的距離。
「唔......！」
被殺了個措手不及，莉莉希娅沒能來得及行動。照這樣下去，她可能會被圓環打中，受到致命傷害。
『維諾斯！』
在心裏呼喚後，像是在表明她心知肚明壹般，王之力被解放了出來。
渾身都迸發着魔力的艾特，在千鈞壹發之際沖到莉莉希娅面前，擋住了圓環。
「為什麽要礙事！而且，還用上了王之力......！」
芭妮拉沖擋住圓環的劍施加着壓力。
壹邊用力壓回去，艾特回答道。
「這還用得着問嗎。因為莉莉希娅是我重要的家人！家人遇到危險自然要幫忙。就這麽簡單！」
「——咕！！」
咯吱作響，芭妮拉咬牙切齒地向後退去。
她暫且和艾特拉開了距離。
「為什麽，你要殺我們。雖然時間不長，可莉莉希娅不也是壹起生活的夥伴嗎。那不就像家人壹樣嗎！」
「才不是！」
芭妮拉即答了。
「我壹直，都是孤零零的壹個分。我沒有梅洛蒂妹妹，更沒有眷屬。所以，我要創造。必須，要由我去創造才行——」
死死瞪着艾特，芭妮拉不斷呼，呼地喘着粗氣。
剩下的兩名帝國兵，便瞄準這個空檔發動了攻擊。
「為盧法斯殿下報仇————！！」
「死吧————————！」
「喂，慢着！」
艾特雖出聲喊他們，可為時已晚。
他們并非如今的芭妮拉的對手。芭妮拉咂了下舌躲過二人的攻擊，接着用圓環斬殺了他們。
現場留下的，僅剩芭妮拉和艾特，莉莉希娅三人——。
「芭妮拉，總之你先冷靜壹下。我只是想和你談談......」
「根本沒有那個必要」
不知從哪裏傳來了這樣壹句話。
同壹時間，還發生了爆炸。
（剛才的是......）
命中地板的同時破裂開來的藍色光彈，引發了爆炸。
艾特朝應該是發射光彈的方向看了過去。
祭壇的上方有壹具作為裝飾的巨大龍型骷髅。而壹名少女從骷髅的頭上跳下，站在了芭妮拉身後。
似人，卻又非人。少女身着黑色禮裙，頭上戴着藍色的花飾，隐約散發出壹股令人毛骨悚然的氣質。
（這家夥......）
簡直就像是和維諾斯相同的存在——。
古魔女壹樣，艾特這麽想到。
就宛如證據壹般，能在她禮裙腹部的開衩處下面，看到和維諾斯肚臍下方相似的發光圖案。
「别被騙了芭妮拉。你的夥伴，不就是這樣被殺死的嗎」
「......！」
仿佛贊同這句話壹般，芭妮拉瞪着艾特舉起了武器。
緊接着。
『艾特，我也出來吧』
聲音剛剛在腦海中想起，艾特的身體便立馬開始發光，維諾斯從中飛了出來。接着她看向方才現身的那名少女，
「好久不見呢，阿莎蕾娅。果然是你嗎」
「............」
就算維諾斯打招呼，被喚作阿莎蕾娅的少女也沒有回答。
她只是壹直死死地瞪着維諾斯。
不過，艾特覺得她們兩個倒是很相似。
容貌仿佛是壹個模子刻出來的，身高也差不多。
連白皙的肌膚都十分相似。
「明明這麽久沒見了，真冷淡啊」
「我讨厭你，淫亂魔女」
阿莎蕾娅第壹次做出了回應。維諾斯壹瞬睜大眼睛，露出了驚訝的表情，不過立馬又出聲笑了起來。
「哈哈哈，看來你還是老樣子呢。我很清楚這點，但還是有個請求。能請你解放那個女孩——芭妮拉嗎？她是我契約者的朋友。我可不想讓她成為你這種死神的犧牲品呢」
「自然，我不能答應你」
被稱作死神的阿莎蕾娅将嘴巴彎成彎月狀，露出了瘆人的笑容。
「況且，是她在渴望我。我們是同類。是以相同的想法連接在壹起的。所以，我才會和她契約」
「同類？你和芭妮拉嗎？」
「我，還有她，都憎恨人類——僅此而已」
「就是說，你利用了她的感情，将她洗腦，引出了過度的能力對吧。說起來，你就是這樣的魔女呢。我才想起來」
很無語似地說完柱子後，
維諾斯将視線轉向芭妮拉，
「芭妮拉，照這樣下去你的身心都會被阿莎蕾娅吞噬殆盡，死在她的手裏。我勸你最好趕快解除契約」
「開什麽玩笑！我怎麽可能，把好不容易得到的王之力丢掉呢！」
這就是芭妮拉的答案。
「話說，倒是艾特快把跟維諾斯的契約解除啊。然後，來做我的眷屬。這樣，我就不用殺你了」
「啥，你說什麽呢——」
「這樣，我和艾特就能成為家人了」
呵呵呵，芭妮拉露出詭異的笑容。
從她的表情之中只能感覺出瘋狂。
「......哎，艾特。你不肯解除和維諾斯的契約嗎。既然這樣，那這件事就沒得談了。我要你死掉，然後成為我的東西。阿莎蕾娅，給我力量！」
「好啊，當然可以」
冷笑壹下的阿莎蕾娅，進入了芭妮拉的身體。
同時芭妮拉的身體迸發出魔力的漩渦。
她毫無疑問是解放了所有王之力。
「明明，你只要老實答應我的請求就好了！」
她瞬間接近了艾特。
艾特用劍擋住了揮下的圓環。
（果然，攻擊比剛才要沉重啊......！）
不愧是借用了魔女的力量。
既然如此，這邊也只能借用了。
「維諾斯！」
「我知道」
維諾斯進入了艾特體内。
然後解放了所有王之力。
這下雙方條件就對等了。
——不。
艾特還有莉莉希娅這位眷屬。
「艾特，我來幫忙！」
「休想！」
芭妮拉看向如此出聲喊道的莉莉希娅，用手蓋住了左眼。
（糟了！）
那和自己用魔眼時的姿勢壹樣，艾特慌忙想要提醒莉莉希娅小心。
此時，維諾斯的聲音響起。
『艾特，告訴你個好消息。阿莎蕾娅的魔眼，對活人是不起作用的。不過——』
正當維諾斯想要繼續說下去的時候，艾特察覺到莉莉希娅身後發生了什麽。
「莉莉希娅，後面！」
「——！？」
莉莉希娅從艾特的聲音中感覺出了什麽，可慢了壹步。
她被盧法斯的巨劍打中，直接被打飛到墻上。
「嗚咕！？」
莉莉希娅身後受到了沖擊。
在睜開眼的同時，她懷疑起自己的眼睛。
「為什麽，你會......？」
她會是這個反應也是正常的。
因為攻擊她的，正是以為已經死了的盧法斯。
只不過，他的樣子有些不對勁。
「——嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷！！」
盧法斯壹邊發出呻吟，壹邊展開追擊。
莉莉希娅向壹旁跳去，躲過了他的劍。
然後，盧法斯又壹邊大吼壹邊展開追擊。
可是，他的劍術就像個外行人。
大開大合，只是在胡亂揮舞。
和在之前的戰鬥中所見的大不壹樣。
艾特對此感到困惑，在心裏問道。
『那是怎麽回事？』
『剛剛我正想說呢。阿莎蕾娅的魔眼，對活人無效——簡單來說，就是将死者化作眷屬進行控制的能力——也就是死靈魔術』
說完這句話之後，其他帝國兵的屍骸，以及散落在現場的人骨都組成骷髅站了起來，将在離艾特稍遠處正和盧法斯戰鬥的莉莉希娅圍了起來。
「真的假的啊......」
看着它們的模樣，芭妮拉說過的關于家人的話同時閃過了他的腦海。
也就是說，她可能是想殺了艾特他們，用自己的能力讓他們成為眷屬——也就是家人。
這樣想的話芭妮拉的言行就說得通了。
（可是，那能算得上家人嗎......！）
他當然不想變成那樣，也不想讓莉莉希娅變成那樣。
最重要的是，得先從盧法斯和帝國兵亡骸，骷髅兵的包圍圈裏把莉莉希娅救出來才行，如此心想的艾特正想展開形容，然而，
「不會讓你礙事的！」
随着這句話，芭妮拉壹起沖了過來。艾特壹邊擋下她的圓環，壹邊沖莉莉希娅喊道。
「那些帝國兵，也和野狗不死族壹樣！他們已經死了，所以不用手下留情！」
「我知道！可是，數量太多了。所以，給我力量！」
「沒問題！」
與莉莉希娅的視線重疊，艾特呼籲着解放了眷屬之力。
随後莉莉希娅的左胸浮現出發光的斑紋，
「謝謝。這下應該就沒問題了」
正如這句話所說，就算不管打倒多少次都會再站起來，但解放了眷屬之力的莉莉希娅在對付帝國兵和盧法斯，骷髅兵時應該不至于受到致命傷。
「——咕，為什麽要給莉莉希娅力量啊！」
「你不也用了王之力嗎，咱們彼此彼此吧。如果不讓我給的話，那你先把那些家夥停下來啊」
「不要，我絕對不要！他們可是好不容易才得到的家人啊！我絕對不會抛棄他們！」
「那種玩意兒可算不上家人吧！」
「煩死了！」
芭妮拉壹邊這麽喊着，壹邊不斷發起攻擊。艾特回避，格擋着這些攻擊，同時在心裏向維諾斯問道。
『喂，該怎麽做才能阻止芭妮拉和那群家夥啊？』
『嗯，這個嘛。只要殺了芭妮拉，應該全都會停下吧』
『你明知道我要的不是這個答案吧！就沒其他辦法了嗎？』
『剩下的就只有讓她解除和阿莎蕾娅的契約了啊......但照現在這情況來看，感覺很難呢。不過——』
『不過什麽啊』
『身為王的艾特，或許能給芭妮拉她想要的東西，說服她哦。過去的王也都數度跨越危機，化敵為友呢。我目睹過好幾次這樣的場景，也聽說過』
『芭妮拉想要的東西，嗎......』
快想。
——不，不對。
這根本不用想。
他想起了在芭妮拉家上面的墳墓前的對話。
在來這座遺迹之前的晚上的對話，還有剛才芭妮拉說過的話，裏面浮現在他的腦海。
『看來你是有主意了呢』
『我找到要點了』
靠這個應該能行。
「聽好了，芭妮拉！」
「幹嘛！」
「你可能以為自己是孤身壹人，但你已經不是壹個人了。我來給你看證據」
如此宣言的艾特壹邊擋開芭妮拉的圓環，壹邊沖向她。
看到這壹幕，莉莉希娅不禁叫道。
「咦！？」
因為，艾特緊緊抱住芭妮拉并吻了她。
「喂，你這是想幹什麽？」
芭妮拉恐怕也壹樣困惑吧。
她好像還沒理解發生了什麽。
只是瞪大眼睛，然後眨巴了兩下。
緊接着，芭妮拉便渾身脫力，地面咣當地響起了兩道聲音。
那是她手裏的圓環發出來的。
「艾，特......？」
「看來是冷靜下來了啊」
将嘴唇松開的艾特，對身上的魔力漩渦消失，眼睛恢復高光，露出驚訝表情的芭妮拉露出了微笑。
艾特身上的魔力漩渦也同樣漸漸退去。
「為什麽，你要......」
「你也來做我的家人吧」
「......家人......？」
「沒錯。不是那些想傀儡壹樣的家夥，還有企圖利用你的家夥。而是像莉莉希娅和梅洛蒂那樣，跟我們成為真正的家人吧」
「可是，你已經有梅洛蒂妹妹和莉莉希娅——」
「說什麽呢。我可是要成為王的男人啊？你不也說過嗎。王擁有後宮是很正常的事。而且，我也絕對不會離你而去」
「啊——」
壹道眼淚，劃過了芭妮拉的臉頰。
「真的嗎？你能答應我，永遠陪在我身邊嗎？」
「當然，絕對會的」
艾特咧嘴壹笑。
因為在兒時失去了朋友，所以芭妮拉對事物就開始有了執着心吧。
從認為可以稱之為朋友的艾特被莉莉希娅搶走之後她就變得執着來看，她毫無疑問很害怕失去。
這句話便是有感而發。
「你已經不是壹個人了。我答應你。所以，現在接觸和阿莎蕾娅的契約吧。那家夥只是想利用你罷了」
覺得現在她肯定能接受的艾特，凝視着她的眼睛這麽請求道。就在此時，腦海中響起了壹道聲音。
『不要再聽他說了。你不是渴望這份力量嗎？』
那道聲音令人不寒而栗，是阿莎蕾娅的聲音。
「對啊......這股力量——只要用了這股力量，死掉的朋友們也會——」
芭妮拉低下頭，身體顫抖起來。
看到她的身體即将再度釋放出強大的魔力漩渦發覺不妙的艾特，用力抱緊芭妮拉繼續說道。
「别聽她的話，芭妮拉！死人是無法復活的！快點解除和阿莎蕾娅的契約！不然的話，你又會——」
『别被他騙了，芭妮拉！你到底要被騙幾次才——』
「——給我閉嘴！！」
芭妮拉用力握緊雙拳，放聲大喊道，
「阿莎蕾娅，我要解除和你的契約」
『什，為什麽——』
從發出光芒的芭妮拉的身體裏，阿莎蕾娅像是被丢出來壹樣被趕了出來。她用手撐地站了起來，表情因不甘而扭曲。
「——為什麽，你要把我......」
就像在追趕她壹樣，維諾斯也從艾特的體内鉆了出來。
「哈哈哈，看來比起你，她選擇了和艾特的緣分呢」
俯視着阿莎蕾娅如此嘲笑她的維諾斯，看向艾特，
「接下來呢，要怎麽辦？」
「怎麽辦是指......」
「當然是說阿莎蕾娅咯。現在她已經解除了和芭妮拉的契約，應該能完成原本的目的了吧？」
「啊，對欸」
原本的目的，指得就是讓莉莉希娅和被封印在這座遺迹裏的古魔女契約。
「你想和我契約嗎？」
詢問的并非艾特，而是當事人莉莉希娅。
「那當然，是不可能的」
怒瞪着她如此回答的阿莎蕾娅用力向後跳去，
「我不相信人類。也不會再與人類為伍。我要做的，就是讓所有人類化作整體，由我來統率。就這麽簡單」
言畢她舉起手後，開衩處後面的紋章便發出光芒，魔力彈襲向了艾特等人。
艾特他們蹬地躲過了攻擊。
「剛才的是......」
本以為在阿莎蕾娅出現的時候飛來的魔力彈，是源于她的魔力，但看來是并非如此。
注意到這點的莉莉希娅，露出了吃驚的表情。
艾特也壹樣。
二人壹邊壹起露出吃驚的表情，壹邊看向漂浮在阿莎蕾娅的頭上，仿佛是活的壹樣還以為只是祠堂裝飾的翼龍骸骨。

(圖片012)

「皮洛斯，是我的朋友。并且，也是令我如今憎恨人類的理由」
從被稱作皮洛斯的翼龍口中，又接連吐出了魔力彈。
這令艾特等人再次後跳，與阿莎蕾娅拉開了相當大壹段距離。
「我被封印在了這座神殿的地下。經由利用我的力量，裝作展現了許多奇迹的宗教領導者之手」
也就是說，為了不讓奇迹是來自古魔女的力量這件事敗露，在宗教運作走上正規之後阿莎蕾娅似乎便被領導者封印，藏匿起來了。
在數百年之後教派分裂，開始内鬥後自己便被所有人遺忘，而且還壹直被封印在這裏。
數年，數百年來，阿莎蕾娅都是孤獨壹人。
「而第壹個出現在我面前的，是壹只還很年幼的翼龍」
阿莎蕾娅也不知道，為什麽眼前會出現壹只年幼的翼龍。
自己被封印的地點，是在由魔術制造出來的封印結界中。
她甚至覺得，那是可能是寂寞難耐的自己用自己的魔力創造出來的。
那只翼龍偶爾會消失不見，但過幾天就又會回到祠堂。
變成寶珠的阿莎蕾娅身旁，似乎成為了它中意的巢穴。
雖然它有壹定的智力，但并不會說話。
自己不能和它契約，也不可能指望它把自己從這裏帶出去。
可即便如此對阿莎蕾娅而言，這只翼龍也在不知不覺間填補了自己的孤獨，成為了唯壹壹個不可替代的朋友。
在壹起度過的數十年間，她為那只翼龍起名皮洛斯，在過去的語言中是朋友之意。
「原本，人類就十分畏懼身為死靈師的我」
同樣在古魔女中也是遭人避諱的存在。
事實上，就連利用了阿莎蕾娅的那幫人類也把她當惡魔來看待。
可是，皮洛斯卻不同。
對阿莎蕾娅來說，皮洛斯是第壹個可以打開心房的對象。
「可以，和皮洛斯壹起度過的日子也宣告結束了，因為那些可恨的人類」
那是和皮洛斯壹起在這裏生活，已經快過去幾百年時候的事。
在長大後，已經擁有成年龍巨軀的皮洛斯正在她旁邊睡覺的時候，有幾名人類來到了這裏。
那是壹群約有十人，以集團進行行動的強者雲集的盜賊。
這本身并沒有什麽問題。
雖然她很讨厭這種人，但如果能把自己從這裏帶出去，她覺得那其實也無所謂。
這樣壹來或許就能把皮洛斯也壹起帶走——到各種地方去轉轉了。
然而，事實并非如此。
『快逃，快！』
阿莎蕾娅焦急地不斷向皮洛斯的大腦傳遞消息。
因為從那些人的交談中她得知，皮洛斯作為翼龍是個十分珍貴的品種，它的鱗片牙齒和眼睛似乎相當值錢。
盜賊們雙眼放光，盯上了皮洛斯。
他們那副模樣毫無疑問是認為皮洛斯就是這座迷宮的寶藏。
阿莎蕾娅慌忙令将自己封印在内的寶珠發光，向盜賊們搭話。
我是古魔女。
和我契約就能給你們王之力。
所以不要殺死那只翼龍。
作為代替摸我就行。
其實，她并不想和人類契約。
然而，盜賊男子卻命令部下們殺了皮洛斯。
他們并不是沒聽說過古魔女和王之力。
盜賊裏也有人知道。
正因如此，那個貪婪的頭目才想要全都得到手。
雖說是古魔女，可在被封印的狀態下應該是無法出手的。
王之力，還有值錢的翼龍，他全都要。
（怎麽會，為什麽會這樣......）
盜賊頭目走向了唯壹壹個朋友壹般的存在即将被殺害，從而陷入絕望的阿莎蕾娅。
就在那時。
「嘎咿咿咿咿！」
皮洛斯為了保護沙雷亞，沖向了盜賊頭目。
『皮洛斯！』
她本以為當它是朋友可能只是自己的壹廂情願，可并不是這樣。
這令阿莎蕾娅十分開心——。
「但是，那便是最後壹面」
緊接着，皮洛斯便被弓箭射殺了。
「怎麽會......」
絕望中的阿莎蕾娅，還是處于被封印的狀態。
但是，術式已經施加了很長時間。
封印魔術在漸漸減弱，被奪走的魔力也在恢復。
那是她為了有朝壹日為了和皮洛斯壹起從這裏逃出去，壹直積攢的力量。
然而，皮洛斯已經不在這個世上了。
已經無所謂了。
阿莎蕾娅被怒火支配，魔力從寶珠中迸發而出。
結果，不要說頭目，在場的所有人都被吹飛到了後方。
（我饒不了他們。皮洛斯，你也壹樣對吧？我不會讓他們拿走你身體的壹根毫毛。所以借我你的力量吧——！）
該說是不幸中的萬幸嗎。
阿莎蕾娅擁有的能力，是控制屍體的能力。
就算她沒法動也能施展能力。
所以阿莎蕾娅控制皮洛斯的亡骸，接連将盜賊殺死，然後再将他們的屍體作為自己的工具，殺光了所有的盜賊。
聽完這些事，艾特想起了剛才襲擊莉莉希娅的那些骷髅兵。他們肯定就是當時的盜賊吧。
「從那之後，我便更加痛恨人類。我不會再相信人類了。人類是應當根絕的存在」
魔法陣出現在阿莎蕾娅的腳邊。
「那家夥，想做什麽......？」
就在莉莉希娅說完這句話之後。
阿莎蕾娅腹部的紋章光輝變得更加強烈，從她舉向天花板的手中，放出了強力的魔力彈。魔力彈壹口氣貫穿好幾個樓層，直達天際。
（好強......）
空氣產生劇烈的震動，連艾特也跟着震了起來。
只不過，剛才那招似乎消耗了她不少魔力。
阿莎蕾娅額頭浮出汗珠，耷拉着肩膀，不斷呼呼地喘着氣。看她這樣子不太像還能動彈得了。
然而，她還有皮洛斯。
『艾特，不能讓她逃了』
維諾斯的話，讓艾特恍然大悟。
他明白了阿莎蕾娅會使用龐大的魔力，打出壹個可以看得見天空的洞的理由。
『以阿莎蕾娅現在的狀態來看，要是讓她和皮洛斯壹起逃了，可能會有其他人類被害吧。我也助你壹臂之力，快阻止她』
話音落下，維諾斯便進入了身體之中。
随後，身體深處便有力量湧現。
（确實要是有這股力量——王之力的話，應該能攔下她）
皮諾斯壹邊拼接骨頭壹邊煽動翅膀，慢慢飛了起來。阿莎蕾娅用手抓住它的腳，也同樣漸漸上浮。
『艾特，動作快！』
『我知道！』
魔力迸發，艾特踢了下地面。
空間就仿佛壹口氣收縮了壹樣。
他壹下子便拉近了三層樓高的距離，
「給我下去！」
水平揮動賦予了維諾斯的魔力——王之力的劍後——命中。
阿莎蕾娅和七零八落的皮洛斯壹起向下墜落，但并沒有摔在地上。她跪地着陸後，
「皮洛斯！」
想要跑向接連摔在地面的翼龍骨頭。
但是，她沒能跑過去。
因為莉莉希娅立馬發動了攻擊。
「答應和我契約。這樣就不用要你的命了！」
「開什麽玩笑，人類！我絕不會和你契約！」
阿莎蕾娅躲開了莉莉希娅的刺劍。雖然她發射魔力彈反擊，但威力并不強。
雖然也是因為解放了眷屬之力的關系，不過阿莎蕾娅用掉了大半魔力也占了很重要的因素。
她完全被莉莉希娅壓制了。
「為什麽，為什麽你不願意和我契約！聽你剛才的話，你渴望的不也是家人嗎！明知被奪走家人的痛苦，卻只想着去奪走别人的家人這也太奇怪了！」
「才不是奪走！我只是想讓大家化作壹個整體成為同伴！靠我的力量，就辦得到！」
「那就根本不算是人類了！只是人偶而已！」
「那又如何！這樣壹來大家就不會分别了！也不會被背叛！除此之外，我已經再也相信不了其他東西了！」
「那麽，你就死在這裏吧」
手持圓環從壹旁跳出來的芭妮拉，朝着阿莎蕾娅的脖子砍去。
阿莎蕾娅在千鈞壹發之際向後跳去躲過了攻擊，，可腹部卻被擊中。
鮮紅的血，從傷口流了出來。
看來魔女的血也是紅的。
「」
「芭妮拉......！」
阿莎蕾娅瞪大的眼睛變得銳利，瞪向了芭妮拉。
芭妮拉輕聲壹笑後說道。
「背叛是我的拿手好戲。而且，這也是對你利用我的感情的報復」
芭妮拉再度砍向阿莎蕾娅。
斬擊&斬擊。二連攻擊令阿莎蕾娅倒向後方。
她似乎已經無法動彈了。
腹部紋章的光輝也已經消失。
「這下，算是告壹段落了吧」
離開艾特身體的維諾斯，這麽說着走向阿莎蕾娅。
她走到旁邊，俯視着口中流下鮮血的阿莎蕾娅，
「雖然傷口已經開始恢復了，不過你應該暫時動不了吧。你想怎麽做？就這樣玩完了？」
「......維諾斯，為什麽你要和人類為伍？你不也遭到人類背叛，被封印——」
「還是我曾經說的那句話。不管我被如何對待，有着憎恨，愛情等諸多感情的人類，都是我最喜歡的存在」
「你真是沒變呢」
阿莎蕾娅嘲笑地笑了。
「那麽，你想把我怎麽樣？消滅我嗎？」
「這個嘛，畢竟古魔女，只有古魔女才能消滅啊。所以要動手，也只能我來了」
「既然如此，讓我好好送皮洛斯壹程。我想說的，只有這些」
「......很遺憾，我不能答應你。雖然你說不會為人類所用，但并沒說不會為我所用吧？」
「難道，你......」
「沒錯。你的力量我就收下了」
「這算什麽。結果，還不是要為人類所用嗎」
「你沒權利拒絕哦。而且我們古魔女，本來就不是壹條心啊」
這麽說着，維諾斯将阿莎蕾娅的上半身撐了起來。
「住手，維諾斯。你這麽做——」
「你可能很抗拒，但我連這樣的你都能接受。永别了，阿莎蕾娅。為了成為我力量壹部分的你，我至少會帶回去皮洛斯的壹塊骨頭的」
「不要——」
露出冷笑的維諾斯，将自己的嘴唇與阿莎蕾娅的嘴唇重疊在壹起。
随後阿莎蕾娅的身體便發出強烈的光芒——。
當光芒消失之後，阿莎蕾娅已經消失得無影無蹤。