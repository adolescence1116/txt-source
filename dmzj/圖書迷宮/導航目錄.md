# CONTENTS

圖書迷宮  
图书迷宫  

作者： 十字静  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E5%9C%96%E6%9B%B8%E8%BF%B7%E5%AE%AE.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/dmzj/%E5%9C%96%E6%9B%B8%E8%BF%B7%E5%AE%AE.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/dmzj/out/%E5%9C%96%E6%9B%B8%E8%BF%B7%E5%AE%AE.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/dmzj/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/dmzj/圖書迷宮/導航目錄.md "導航目錄")




## [第一卷](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7)

- [序1](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00020_%E5%BA%8F1.txt)
- [序2](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00030_%E5%BA%8F2.txt)
- [【距離喪失記憶　還剩九百三十六頁】](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00040_%E3%80%90%E8%B7%9D%E9%9B%A2%E5%96%AA%E5%A4%B1%E8%A8%98%E6%86%B6%E3%80%80%E9%82%84%E5%89%A9%E4%B9%9D%E7%99%BE%E4%B8%89%E5%8D%81%E5%85%AD%E9%A0%81%E3%80%91.txt)
- [【距離喪失記憶　還剩六百三十頁】](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00050_%E3%80%90%E8%B7%9D%E9%9B%A2%E5%96%AA%E5%A4%B1%E8%A8%98%E6%86%B6%E3%80%80%E9%82%84%E5%89%A9%E5%85%AD%E7%99%BE%E4%B8%89%E5%8D%81%E9%A0%81%E3%80%91.txt)
- [【距離喪失記憶　還剩三百五十六頁】](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00060_%E3%80%90%E8%B7%9D%E9%9B%A2%E5%96%AA%E5%A4%B1%E8%A8%98%E6%86%B6%E3%80%80%E9%82%84%E5%89%A9%E4%B8%89%E7%99%BE%E4%BA%94%E5%8D%81%E5%85%AD%E9%A0%81%E3%80%91.txt)
- [【距離喪失記憶　還剩三百三十一頁】](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00070_%E3%80%90%E8%B7%9D%E9%9B%A2%E5%96%AA%E5%A4%B1%E8%A8%98%E6%86%B6%E3%80%80%E9%82%84%E5%89%A9%E4%B8%89%E7%99%BE%E4%B8%89%E5%8D%81%E4%B8%80%E9%A0%81%E3%80%91.txt)
- [【距離喪失記憶　還剩二百六十一頁】](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00080_%E3%80%90%E8%B7%9D%E9%9B%A2%E5%96%AA%E5%A4%B1%E8%A8%98%E6%86%B6%E3%80%80%E9%82%84%E5%89%A9%E4%BA%8C%E7%99%BE%E5%85%AD%E5%8D%81%E4%B8%80%E9%A0%81%E3%80%91.txt)
- [【距離喪失記憶　還剩三十八頁】](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00090_%E3%80%90%E8%B7%9D%E9%9B%A2%E5%96%AA%E5%A4%B1%E8%A8%98%E6%86%B6%E3%80%80%E9%82%84%E5%89%A9%E4%B8%89%E5%8D%81%E5%85%AB%E9%A0%81%E3%80%91.txt)
- [【距離喪失記憶　還剩九頁】](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00100_%E3%80%90%E8%B7%9D%E9%9B%A2%E5%96%AA%E5%A4%B1%E8%A8%98%E6%86%B6%E3%80%80%E9%82%84%E5%89%A9%E4%B9%9D%E9%A0%81%E3%80%91.txt)
- [後記](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00110_%E5%BE%8C%E8%A8%98.txt)

