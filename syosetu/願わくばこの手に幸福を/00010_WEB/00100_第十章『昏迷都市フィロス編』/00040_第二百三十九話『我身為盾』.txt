“怎麼樣，卡麗婭。我能把背托付給你嗎？”
卡麗婭的嘴唇微微地動了起來。那個表情，就像是在看準我似的，浮現出淡淡的笑容。
“——知道了。既然你這樣希望，那麼此身就是你的盾牌，路易斯。”
話雖如此，卡麗婭的銀眼卻還一直盯著我。

剛才還被蘊含在她眼神中的殘酷和危險之類的東西總算消失了，但怎麼說好呢，這次她的眼神裡卻反而帶有一種嗜虐性的熱度，她眼裡浮現出來的那種感覺正是我以前經常看到的神色。

討厭，我有預感，有很討厭的預感。小小的嘴唇，在眼前再次搖晃，讓我輕輕地眨了眨眼。
“那麼，你該不會以為只用一句話就結束了吧，路易斯。你以為光用嘴就能把我擺平嗎？”
那樣的話打動了耳朵，瞬間。卡麗婭的手指包裹著我的臉頰，有種融入了明確的力量的感覺，就好像從現在開始，緊緊握住我的臉的那種力量。
騙人的吧，喂。這是逼近的明確的死亡感觸，有近乎冷汗的東西，從我的背上流下。我聽到了全身神經急劇收緊的聲音。
“聽好了，路易斯。別把我和別的女人混在一起。我並沒有被你一句話就迷惑的那麼天真。”
卡麗婭將我的全身壓在椅子上，好好地述說著。一聽到那句話，我就覺得臉頰發酸。

原來如此，我好像不僅僅是踩到了卡麗婭的逆鱗，甚至讀錯了她的內心。卡麗婭好像對我抱著以執著為名的深厚的感情。那個感情現在積蓄著與城壁都市那時無法比擬的濃度，甚至已經作為粘稠的黑氣顯露出來。

在曾經的旅途上看到的卡麗婭，是否也曾感受過如此之多的東西呢？
牙齒歪斜，無法咬合，發出奇怪的聲音。我問我自己的內心是怎麼回事。

對於卡麗婭那凶猛窺視的情感，只有真正的恐怖才是應該記住的東西。因為她的感情會將人咽下，然後就那樣咬碎。

儘管如此，我的心似乎並不感到恐怖。倒不如說，從心中快要溢出的完全相反的感情。
“……那麼，怎麼辦才好呢？我的盾，你想要什麼保證呢？你想問的是這個吧？”
卡麗婭聽到這種說法，嘴唇波動著笑了，在臉頰上畫著美麗的線條。
如果只是像現在這樣笑著，倒不如說覺得很美。因為那個表情的背面是貨真價實的殘酷且有嗜虐性的意志，所以這個女人很可怕。

恐怕，卡麗婭所要求的是寫著剛才話語的契約書，或是其他以明確的形式記錄承諾的什麼東西吧。不管怎麼說，我好像完全失去了卡麗婭對我的信用，這真是無比傷心的事。
嗯，但是，那樣的話也不錯吧。用筆在羊皮紙上記錄那種事，不就能輕而易舉地把現在的境況給解開嗎？如果因此卡麗婭就滿足了的話。

這樣想著，縮著肩膀，然後和卡麗婭一樣浮現出微笑的瞬間。
什麼東西進入了嘴裡，纖細柔軟的某種東西，強行推開嘴唇和牙齒，就那樣貫穿到我的喉嚨深處。

視野忽明忽滅。喉嚨好像有什麼東西在逆流。
發生了什麼？身體被刺激排斥，打算動彈，卻被卡麗婭壓住不能動。

一瞬間，一邊吐出嗚咽，一邊想辦法睜開眼睛，看著正面。從嘴裡塞進去的東西的真面目，看到了。
那是卡麗婭的白色手指。她眼中浮現出嗜虐的顏色，用手指嘲弄著我的舌頭和喉嚨。口中彌漫著鐵一般的氣味和味道，那是血的味道。
“喝吧，路易斯。血脈交合是古老的約定方法，不過卻是簡單易懂的契約。什麼呀，如果你也打算遵守承諾的話，就沒有問題了吧？”
我倒是喝什麼都可以。卡麗婭現在硬是往我喉嚨裡注入血，可能是因為心情吧。

從卡麗婭手心蔓延的傷口中流出的血液，被強行塞進我的口和喉嚨中。鐵的味道令人討厭，在舌頭上蔓延開來。
血脈交合。確實在古代，這應該是上流階層的家庭之間使用的儀式。

那時王權還很脆弱，所有的上流階級的人們，都是蛇蠍心腸，互相算計的時代。在那種時候能夠找到值得信任的人，就像在大海中尋找一條小船，在森林中尋找一片葉子一樣。

但是，在這種情況下，有時也必須和別人聯手，把背托付給別人。在這種時候，上流階級的貴族們所使用的是被稱為血脈交合的約定儀式。

被稱作貴族的人，比任何事項都優先的重視著驕傲和血液。對於平民的我來說，他們的這種拘泥恐怕是我無法想像的吧。他們無處不在地從自己的血液中消除紛爭，甚至瘋狂地追求著血脈的純粹性。

正是因為他們如此注重血液和驕傲，才有了這種被稱為血脈交合的契約。交換契約的人互為對方獻血，然後將血喝下。由於這種契約，那些人已經成為了血脈相近，有著親屬關係的血盟者。

老實說，這個契約到底哪裡有用，我實在難以理解。不過，即使如此，這個儀式應該也確實連貴族之間也被慎重對待。以前只是以此聯繫血盟者而已，現在，以這個方法建立相互之間的深交的貴族還有很多。

當然，在實際舉行儀式時，並不是強行把手伸進對方的嘴裡的，而是相互之間往葡萄酒中滴下一滴血。
果然，這個名叫卡麗婭的女人還是有一兩個地方的常識，不，恐怕是幾乎全部的常識都偏離了吧。

聽到了我好幾次喉嚨鳴叫的聲音，卡麗婭感到很滿足，就那樣輕輕地拔出手指，在耳邊低聲私語“不要動”。我幾乎覺得抵抗已經沒有意義了，所以閉上雙眼，將體重放在椅子上，靜靜地吐氣，不由得想到了接下來會發生的事。

正如我所想像的那樣，下一個瞬間臉頰上出現了一絲的疼痛。是刀刃割破肉的感覺，像麻痺一樣的尖的刺激感。血慢慢地順著臉頰流下。
這次，很難說出口的，奇怪的感覺碰到了臉頰，不禁咬牙。

該怎麼形容呢？有一點暖和，混雜著瘙癢的觸感。那個奇怪的感覺和觸及耳朵的卡麗婭的呼吸，讓我禁不住咽了口唾沫。
“聽好了，路易斯。你把後背托付給我。我接受了你的話。這是主從之間的契約。”
卡麗婭在耳邊低聲私語。垂下臉頰的血，輕輕地被卡麗婭的舌頭舔了。低聲細語的聲音和那個觸感，不知不覺地讓我脊梁發寒。
“如果今後你撕毀了契約，像以前一樣一個人消失在某處——不管那裡是天涯還是海角，我都一定要找到你，把你的身體弄得再也無法一個人出走。”

過了一會兒，卡麗婭的吐息悄悄地從臉頰離開了。重量從身體中消失，四肢終於得到了解放。臉頰上滴下的血，已經完全流到了身體上。

睜開眼皮，卡麗婭毫不厭倦地望向這邊。但是那個表情，比進入帳篷的時候溫柔了許多。
小小的嘴唇在眼前張開。

“——路易斯。我是你的盾，你絕對不能放手。如果放手的話，盾可能會把主人吃掉的。”
卡麗婭開玩笑地說。快要被吸入了的銀眼，快樂的凝視著這邊。我微微吐氣，張開嘴。
這傢伙真的是本質上不變的女人啊。
“——卡麗婭，希望大家都注意不要被彼此甩了啊。”
我為了配合卡麗婭，故意露出笑容，這樣說道。