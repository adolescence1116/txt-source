經過一系列機緣巧合，被「妖精之道」拋飛的葛蓓莉婭與盧卡、賽拉維、行商人男性、莎托、赫雷斯商會的會長千金艾絲蒂爾一行人意外相遇了。

原本在黑暗中摸索著茜兒去向的一行人，意外輕鬆地獲得了線索。不過，雖說有驅使著「真龍」的盧卡在，但就如今的所在地「愚者的沙海」來說，要一下子斜穿整個大陸卻是不可能的（這麼做的話就會侵犯到許多中立國與古拉維奧爾帝國的敵對國魔人國托魯米特等國的領空了。）

於是眾人只好花上一巡周的時間穿越「愚者的沙海」，奔赴吸血鬼支配的國度，新尤斯大公國尋求熟人──新尤斯大公國的支配者海爾公主的援助。

「我有一個條件。啊啊，盧卡斯公子將自己的血獻給我、或是將來把我和茜兒大人共同迎為妻子的話，今後我也將無條件提供幫助。」
「絕對要拒絕她！！」

儘管和艾絲蒂爾起了爭執，

「嚯嚯嚯嚯嚯，真是個倔強的姑娘呢。我中意你了。如果你願意的話可以做我的眷屬，我將給予你永恒的生命哦！」

公主只是莞爾一笑，挺胸道。

「雖然海爾公主你沒有什麼能夠炫耀的胸部就是了。」

小聲地潑了盆冷水的葛蓓莉婭。
下一瞬間，葛蓓莉婭的身體被一發無詠唱魔法炸出了宮外，途中撞碎了好幾道牆壁。

「──所以才說貧乳心胸狹隘啊！果然人的器量是由胸膛的偉岸程度決定的！學學克拉拉大人的不拘小節⋯⋯果然還是太強人所難了嗎，對這斷崖絕壁，強行拔高的胸部來說～～～～～～～～！！！」

從遙遠的另一端傳來葛蓓莉婭元氣滿滿的誹謗與中傷。

經過這麼一番雞飛狗跳地交涉後，一行人得以使用國內的「轉移門」，抄近路一口氣通過了東海岸的近海，從公海北上進入了古拉維奧爾帝國。

「咕啊～～～～～～～⋯⋯噗嗚～～～～⋯⋯嘔～～～～～⋯⋯」

不久，由於艾絲蒂爾莫名其妙地開始暈船，只好走走停停地穿越廣袤的古拉維奧爾帝國──因為這個原因，趕路的速度也下降了大半，直接導致盧卡回國的消息在各地的通報下傳入了帝都。

「之前坐在澤克斯背上的時候還好好地，怎麼一出海就犯暈了⋯⋯？」

一邊簡單地為其施加治癒術，賽拉維一邊思索了起來。

「或許是精神上的問題吧。艾絲蒂爾以前坐船時也是暈得厲害呢。」

盧卡由於想起了過去的情況，於是加入了對話，

「應該和心態有關。這種時候放鬆下來數數云什麼的，不知不覺就會沒事的。」

這時葛蓓莉婭為了幫上忙，提出了微妙偏離了要點的建議。
一路上保持著這種節奏──

要說的話，原本是打算就這樣穿過帝國直接前往「闇之森」的，但由於為了迎接帝國的直系帝族與帝國首屈一指的富豪海運王赫雷斯伯爵的千金的歸來，飛龍部隊已經等候多時，無法視而不見直接路過，於是盧卡一行人不得不在帝都接受了五天假托報告會之名的盤問。
多虧了有當今皇帝陛下與父親艾魯瑪公爵（擁有第一帝位繼承權的皇子）囑托在先，一行人只是多少受了點叮囑和教育便得到了解放。然而──

「這麼久才難得回來一趟，我想聽聽盧君這段時間都做了些什麼嘛～」

面對自家的天然母親的請求也不可能無視，就當是順便招待同伴們來家裡休息，盧卡把除同樣回到家裡的艾絲蒂爾以外的人帶到帝都的本宅中待了三天。

「⋯⋯嘛啊，反正也要在帝都收羅物資和情報。」

對於試圖讓自己釋懷的盧卡，葛蓓莉婭一臉「我懂」的表情表同意道：

「確實。如果有世界規模的異變發生的話，那麼克拉拉大人肯定就在那裡，反過來說如果什麼什麼都沒發生的話，就說明應該還在『闇之森』無所事事。所以為了不再搞錯方向，在這裡先搜集好情報才是明智之舉呢。」

除盧卡外的所有人都對這段話默默地表示了同意。

「不是，你們那匪夷所思的確信太不正常了！就我所知，茜兒一名是比任何人都要端莊且熱愛和平，心地純正的淑女啊！」

對於盧卡的辯護，

「雖然你說得確實沒錯啦──」說著，大大地將頭歪向一邊的葛蓓莉婭。

「該說是雖然為人端莊但卻不計後果，搞得困難總是接踵而來嗎。」

摸著下巴分析的賽拉維。

「那傢伙根本就是台風眼！明明身周總是被暴風雨關照，自己卻是面朝大海春暖花開，給人添麻煩也要有個度啊！！」

憤慨萬分的艾絲蒂爾。

「這個說法很不錯嘛。確實是給人這種感覺的。」

暢快地笑出了聲的行商人。

「偏偏還這麼無敵，就連為她擔心都是多餘的喵。」

進行總結的莎托。

───

對於一伙人不知道該說是給予了完全的信任，還是固著在了奇怪的認知上的發言，盧卡從中感覺到了莫名能打動人的說服力，於是選擇了沉默。

之後，值得慶幸的是，盧卡從自己的父親艾魯瑪公爵那裡得知了最近並沒有發生震撼大陸的騷動。於是向其他人傳達了就這樣前往「闇之森」的方針，並取得了大家的同意。

就這樣，盧卡在三天裡想辦法完成了對母親卡洛麗娜的說明與說服，做好了出發的準備，就在這時：

「接下來就是和茜兒醬的婚禮了吧？還是說，突然間就抱個孫子回來⋯⋯唔呼呼呼。」

抱著已經一歲大了的妹妹安潔莉娜前來送行的卡洛麗娜露出了意有所指的微笑。

正因為這並不是在開玩笑，所以才讓人犯愁。
身旁一臉壞笑的父親，身為帝國公爵，擁有第一帝位繼承權的艾魯瑪：

「是啊。真是期待呢。」

裝作什麼都不知道的樣子故意表示了同意。早已習慣了父母的不按套路出牌的盧卡，在義務性地與家人打了聲離別的招呼後，找到悠閑地暫居在寬敞的公爵府（或許應該說是公爵城）的庭園裡的「真龍」的成龍澤克斯，確認了一下裝在其背上的鞍與行李（關於這方面，因為家裡還養著飛龍，所以算是手到擒來），以及沒有什麼不舒服的地方和不足之處後，對澤克斯下了出發飛往「闇之森」的指令。

另外，和抵達帝都時不同，這次同行人數減至了兩人。
因為種種緣故艾絲蒂爾和迷之行商人選擇了留在帝都。

艾絲蒂爾那邊自不必說，在她的父親赫雷斯伯爵的身為伯爵家的千金不應該胡亂行動的主張下，被禁止了外出，幾乎被半強制地監禁在了帝都的宅區。

而另一方面，黑髮的迷之行商人青年則是：

「難得來帝都一趟，我準備在這裡做些買賣。誒？臨陣脫逃？哈哈哈哈哈哈，你在嗦什麼啊。──肥見了，莎托。之後拜多了～！」

在聽到要前往的地方是位於「闇之森」的蕾吉娜的茅屋，並且連傳說中的聖女斯諾和森林的守護神「黃金龍王」和『精靈王』以及『獸皇』齊聚一堂後，明顯開始不情不願的行商人在就要出發時第一個退出了。

「要逃跑我倒是無所謂啦，但是老板，這應該屬於出差補貼和特別補貼的適用對象吧喵？」
「當然，我會積極考慮的。」
「這絕對是不打算給了吧喵！？」

於是，到最後變成了盧卡、賽拉維、葛蓓莉婭、莎托四人，一行人早上從帝都出發，用上了從帝都通往西之開拓村的轉移門，終於在中午時到達了「闇之森」

一邊為漫長旅途的結束而鬆了口氣，盧卡一邊讓澤克斯盤旋在新建好的蕾吉娜的茅屋的上空。看到煙囪裡有煙冒出，盧卡在為確實有人在這件事安心地撫了撫胸口後，回過頭對其他人說道：

「看來這次似乎沒有搞錯的樣子。不過也不能直接就這麼降落到茅屋的旁邊，所以我準備在稍遠一些的地方降落，等一下走在森林中時稍微注意一下。」

過去──其實是四年以前，曾經搭著父親艾魯瑪的飛龍便車來過一次的盧卡根據來時的記憶，讓澤克斯轉向了位於森林不遠處的草原。

「這就是傳聞中著名的魔境『闇之森』嗎？」

賽拉維一副感慨頗深的樣子凝望著佔據了全部視野的蔥郁森林。

「要去剛才那間小屋嗎喵？可你們知道要怎麼從這裡過去嗎喵？」

對於莎托理所當然的疑問，盧卡有些心裡沒底地看向了葛蓓莉婭那邊。

「我來這已經是四年之前的事了，而且茅屋的位置好像也不一樣了⋯⋯」

「交給我吧。我萬能女僕葛蓓莉婭會用最短的路線把你們帶到茅屋那邊的！」

拍著胸口的葛蓓莉婭那自信滿滿的態度帶給眾人的只有不安。

「我們先往這邊走吧。」

「那邊不是森林的相反方向嗎！」

果然，葛蓓莉婭不負眾望地指著相反方向準備帶路，賽拉維果斷站出來質疑。
被打斷了的葛蓓莉婭一副「哈啊⋯⋯真是拿你沒辦法」的樣子用鼻子哼地發出一聲嗤笑，說：

「呵，所以才說愚民你膚淺啊。往這邊走前面應該就是西之開拓村了。而且，這個時候艾蓮前輩應該已經回來了。讓前輩來帶路不就好了嗎。」

一開始就沒有打算要自力更生。

「你啊，之前不是還一通豪言壯語說自己搭載了地圖繪製功能嗎？」

對於目不轉睛地盯著自己的賽拉維的追問，葛蓓莉婭也不知道是吹了什麼風，

「這座森林的磁場和空間不斷在變動，而且地形本身也時時在改變，所以要準確記錄下來是不可能的～。而且根據公會的判斷這裡動不動就有Ａ級魔物出沒。這方面，如果有艾蓮前輩在的話，就能讓Grandmaster的使魔Ｓ級魔獸來給我們領路，這樣才讓人放心啊。」

竟然出乎意料地給出了具有說服力的理由。

既然都說到了這個份上，也只能按照她說的來了。雖然麻煩，但也只能從這裡往回走前往西之開拓村了。
如果坐著「真龍」過去的話八成會引起騷動，所以大概只能走路過去了吧。

就在所有人這麼做好覺悟時，澤克斯突然警惕地發出了低吼。

「『『『？？？』』』」

下個瞬間──

「咦，這不是擔任茜兒大人侍女的自動人偶嗎！！感知到異常的氣息所以就過來看一下，這就是所謂的緣分吧！你沒事？」

肩上扛著一柄巨大的斧頭，身高遠超兩米，披著看上去就像是由一塊塊鐵板拼接而成的超重量級黑色甲胄的死靈騎士巴爾托洛梅，一副就像是在散步途中的樣子，語氣輕鬆地向葛蓓莉婭搭話。

「『『死，死，死靈騎士！？！』』」

「喂──，來的正好啊。」

一片嘩然的盧卡、賽拉維、莎托，與他們形成了鮮明對比的是，葛蓓莉婭一副氣定神閑的樣子，向巴爾托洛梅舉起一隻手打起了招呼。
見狀巴爾托洛梅也抬起一隻手表示回應。

看著兩人（兩具？）彼此之間沒有隔閡的樣子，不明就裡的三人面面相覷。

「那個，葛蓓莉婭⋯⋯小姐？你和那邊的死靈騎士⋯⋯大人認識？」

見到被公會判定為ＳＳ級魔物的「真龍」澤克斯在看到死靈騎士後明顯畏縮的樣子，盧卡斟酌著語句問道。

「啊啊，這是自稱克拉拉大人的守護騎士的巴爾托洛梅。──另外這邊是古拉維奧爾帝國現任皇帝的孫子盧卡殿下、愚民和附帶的商人。」

簡單地說明後，又像是順帶一般將盧卡、賽拉維、莎托依次介紹了一遍。

「呵呵。是你們啊！時常聽茜兒大人提起你們！在下正是天壤無窮長盛不衰，享譽四方的真紅帝國光榮的宮殿騎士，巴爾托洛梅！！」

威風堂堂地報上名號的巴爾托洛梅。
原本在聽到古拉維奧爾帝國的帝孫後應該多少會意識到什麼，但似乎只是被統一劃歸為了「茜兒的朋友」

「你來得剛好，能帶我們去茅屋嗎？」
「舉手之勞。不過現在魔女大人和瑪雅大人可不在茅屋裡。」
「其他人和克拉拉大人呢？」
「其他人，是說公主和宰相⋯⋯不，是說聖女大人與『黃金龍王』大人與『精靈王』大人嗎？她們第二天就回去了。茜兒大人則是自從那天聽了聖女大人的囑托後便出門了，至今未歸。」

面對對方娓娓道來的事實，盧卡緊張了起來。

「到，到哪裡去了！？茜兒到哪裡去了？」

「在哪呢？魔女大人或許會知道詳情，不過應該是沒有離開『闇之森』的吧。」

不知是演技，還是真不知道，巴爾托洛梅微微聳肩給出了曖昧的回答。

在下了無論如何都要先去一趟蕾吉娜那邊，否則就無法把握情況的判斷後，一行人跟著轉身向「闇之森」走去的巴爾托洛梅，齊齊邁出了腳步。

「說起來，以青六為代表的『龍牙戰士』他們還在動嗎？」
「嗯。還很精神。最近似乎還開始自給自足地種起了田，進行物物交換，而且還獨自與西之開拓者進行著交流。」
「這也太無法無天了吧。明明我根本就沒有下過這樣的命令。」
「嗯。似乎自從侍女大人不在以後，便謳歌著閑適的生活呢。」

聽著葛蓓莉婭和巴爾托洛梅的閑聊，「肯定是趁煩人的上司不在，趕緊韜光養晦吧」，一行人不約而同地產生了同樣的想法。